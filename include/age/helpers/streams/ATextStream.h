/*
Copyright (C) 2018 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_STREAMS_ATEXTSTREAM_H_INCLUDED
#define AGE_HELPERS_STREAMS_ATEXTSTREAM_H_INCLUDED

#include <string>

#include "age/helpers/streams/ABaseStream.h"

//=============================================================================
class ATextStream : public ABaseStream
{
public:
// Interface
    ATextStream() {}
    ATextStream(ATextStream&&) noexcept = default;

    ATextStream& operator = (ATextStream&&) noexcept = default;

    ATextStream& operator << (const std::string& str);
    ATextStream& operator << (const char* str);
    ATextStream& operator << (char c);
    ATextStream& operator << (bool value);
    ATextStream& operator << (float value);
    ATextStream& operator << (double value);
    ATextStream& operator >> (std::string& str);
    ATextStream& operator >> (char& c);
    ATextStream& operator >> (bool& value);
    ATextStream& operator >> (float& value);
    ATextStream& operator >> (double& value);

    template<class V, class = std::enable_if_t<std::is_integral<V>::value>>
    ATextStream& operator << (V value)
    {
        std::string buffer = std::to_string(value);
        return operator<<(buffer);
    }

    template<class V, class = std::enable_if_t<std::is_integral<V>::value>>
    ATextStream& operator >> (V& value)
    {
        std::string buffer;
        operator>>(buffer);
        value = static_cast<V>(std::stoll(buffer));
        return *this;
    }

private:
// Methods
    void skipSpaces();
    void readWord(std::string* word);

    template<class R>
    std::string realToString(R value)
    {
        std::string result = std::to_string(value);

        std::size_t sep = result.find('.');
        if(sep == std::string::npos)
            return result;

        std::size_t last = result.find_last_not_of('0');
        if(last != sep)
            last++;

        return result.erase(last, std::string::npos);
    }

private:
// Deleted
    ATextStream(const ATextStream&) = delete;
    ATextStream& operator = (const ATextStream&) = delete;
};
//=============================================================================
#endif // AGE_HELPERS_STREAMS_ATEXTSTREAM_H_INCLUDED
