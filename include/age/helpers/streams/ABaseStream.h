/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_STREAMS_ABASESTREAM_H_INCLUDED
#define AGE_HELPERS_STREAMS_ABASESTREAM_H_INCLUDED

#include <memory>

#include "age/helpers/streams/ABaseStreamSource.h"
#include "age/helpers/streams/ANullStreamSource.h"
#include "age/core/types/AAliases.h"

//=============================================================================
class ABaseStream
{
public:
// Interface
    ABaseStream() : src_(new ANullStreamSource()) {}
    ABaseStream(ABaseStream&&) noexcept = default;

    ABaseStream& operator = (ABaseStream&&) noexcept = default;

    template<class S, class... Args>
    void setSource(Args... args)
    {
        src_ = std::make_unique<S>(std::forward<Args>(args)...);
    }

    bool isEof() const
    {
        return src_->isEof();
    }

    int64 setPos(int64 offset,
        ABaseStreamSource::StreamWhence whence = ABaseStreamSource::SW_BEGIN)
    {
        return src_->setPos(offset, whence);
    }

    int64 getPos() const
    {
        return src_->getPos();
    }

    void write(const void* data, int64 len) { src_->write(data, len);         }
    void read(void* data, int64 len)        { src_->read(data, len);          }
    byte showCurrentByte()                  { return src_->showCurrentByte(); }
    void skipCurrentByte()                  { src_->skipCurrentByte();        }

    int64 getBytesAvailableCount()
    {
        return src_->getBytesAvailableCount();
    }

private:
// Data
    std::unique_ptr<ABaseStreamSource> src_;

private:
// Deleted
    ABaseStream(const ABaseStream&) = delete;
    ABaseStream& operator = (const ABaseStream&) = delete;
};
//=============================================================================
#endif // AGE_HELPERS_STREAMS_ABASESTREAM_H_INCLUDED
