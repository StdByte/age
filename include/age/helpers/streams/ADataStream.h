/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_STREAMS_ADATASTREAM_H_INCLUDED
#define AGE_HELPERS_STREAMS_ADATASTREAM_H_INCLUDED

#include <algorithm>
#include <memory>
#include <string>

#include "age/core/types/AAliases.h"

#include "age/helpers/streams/ABaseStream.h"

class ARect;
class AXy;

//=============================================================================
class ADataStream : public ABaseStream
{
public:
// Helpers
    enum ByteOrder
    {
        BO_BIG_ENDIAN,
        BO_LITTLE_ENDIAN
    };

public:
// Methods
    static ByteOrder getSystemByteOrder();

public:
// Interface
    ADataStream();
    ADataStream(ADataStream&&) noexcept = default;

    ADataStream& operator = (ADataStream&&) noexcept = default;

    void      setByteOrder(ByteOrder order) { order_ = order; }
    ByteOrder getByteOrder()                { return order_;  }

    ADataStream& operator << (bool value);
    ADataStream& operator << (const AXy& xy);
    ADataStream& operator << (const ARect& rect);
    ADataStream& operator << (const std::string& str);
    ADataStream& operator >> (bool& value);
    ADataStream& operator >> (AXy& xy);
    ADataStream& operator >> (ARect& rect);
    ADataStream& operator >> (std::string& str);

    template<class V, class = std::enable_if_t<std::is_fundamental<V>::value>>
    ADataStream& operator << (V value)
    {
        writeInOrder(value);
        return *this;
    }

    template<class V, class = std::enable_if_t<std::is_fundamental<V>::value>>
    ADataStream& operator >> (V& value)
    {
        readInOrder(value);
        return *this;
    }

private:
// Methods
    template<class V>
    void writeInOrder(V value)
    {
        const byte* data = reinterpret_cast<const byte*>(&value);
        int64       len  = sizeof(V);

        if(order_ == systemOrder_)
        {
            write(data, len);
            return;
        }

        V buffer;
        std::reverse_copy(data, data + len, reinterpret_cast<byte*>(&buffer));
        write(&buffer, len);
    }

    template<class V>
    void readInOrder(V& value)
    {
        byte*  data = reinterpret_cast<byte*>(&value);
        int64  len  = sizeof(V);

        read(data, len);

        if(order_ != systemOrder_)
            std::reverse(data, data + len);
    }

private:
// Data
    static ByteOrder systemOrder_;

private:
// Data
    ByteOrder order_;

private:
// Deleted
    ADataStream(const ADataStream&) = delete;
    ADataStream& operator = (const ADataStream&) = delete;
};
//=============================================================================
#endif // AGE_HELPERS_STREAMS_ADATASTREAM_H_INCLUDED
