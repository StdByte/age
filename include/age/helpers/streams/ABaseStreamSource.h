/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_STREAMS_ABASESTREAMSOURCE_H_INCLUDED
#define AGE_HELPERS_STREAMS_ABASESTREAMSOURCE_H_INCLUDED

#include "age/core/types/AAliases.h"

//=============================================================================
class ABaseStreamSource
{
public:
// Helpers
    enum StreamWhence
    {
        SW_BEGIN,
        SW_CURRENT,
        SW_END
    };

public:
// Interface
    virtual ~ABaseStreamSource() {}

    virtual bool isEof() const = 0;

    virtual int64 setPos(int64 offset, StreamWhence whence) = 0;
    virtual int64 getPos() const                            = 0;

    virtual void write(const void* data, int64 len) = 0;
    virtual void read(void* data, int64 len)        = 0;

    virtual byte showCurrentByte() = 0;
    virtual void skipCurrentByte() = 0;

    virtual int64 getBytesAvailableCount() = 0;
};
//=============================================================================
#endif // AGE_HELPERS_STREAMS_ABASESTREAMSOURCE_H_INCLUDED
