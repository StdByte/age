/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_STREAMS_AFILESTREAMSOURCE_H_INCLUDED
#define AGE_HELPERS_STREAMS_AFILESTREAMSOURCE_H_INCLUDED

#include <memory>
#include <string>

#include "age/core/system/AFile.h"
#include "age/core/types/AAliases.h"

#include "age/helpers/streams/ABaseStreamSource.h"

//=============================================================================
class AFileStreamSource : public ABaseStreamSource
{
public:
// Interface
    AFileStreamSource(const std::string& path,
        int32 mode = AFile::OM_READ_WRITE);
   ~AFileStreamSource();

    bool isEof() const override;

    int64 setPos(int64 offset, StreamWhence whence) override;
    int64 getPos() const                            override;

    void write(const void* data, int64 len) override;
    void read(void* data, int64 len)        override;

    byte showCurrentByte() override;
    void skipCurrentByte() override;

    int64 getBytesAvailableCount() override;

private:
// Helpers
    enum SourceMode
    {
        SM_NONE,
        SM_READ,
        SM_WRITE
    };

private:
// Methods
    void applyMode(SourceMode mode);

private:
// Data
    std::unique_ptr<class AFileStreamSourceData>   data_;
    std::unique_ptr<class AFileStreamSourceReader> reader_;
    std::unique_ptr<class AFileStreamSourceWriter> writer_;
    SourceMode                                     mode_;

private:
// Deleted
    AFileStreamSource(const AFileStreamSource&) = delete;
    AFileStreamSource& operator = (const AFileStreamSource&) = delete;
};
//=============================================================================
#endif // AGE_HELPERS_STREAMS_AFILESTREAMSOURCE_H_INCLUDED
