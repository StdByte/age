/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_STREAMS_ANULLSTREAMSOURCE_H_INCLUDED
#define AGE_HELPERS_STREAMS_ANULLSTREAMSOURCE_H_INCLUDED

#include "age/helpers/streams/ABaseStreamSource.h"

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/helpers/AHelpersExceptions.h"

//=============================================================================
class ANullStreamSource : public ABaseStreamSource
{
public:
// Interface
    ANullStreamSource() {}

    bool isEof() const override
    {
        return true;
    }

    int64 setPos(int64 offset, StreamWhence whence)
    {
        if(offset == 0)
            return 0;

        throw AIoException(A_EXPOINT);
    }

    int64 getPos() const
    {
        return 0;
    }

    void write(const void* data, int64 len) override
    {
        if(!data)
            throw ANullptrException(A_EXPOINT);
    }

    void read(void* data, int64 len) override
    {
        if(!data)
            throw ANullptrException(A_EXPOINT);

        if(len > 0)
            throw AIoException(A_EXPOINT);
    }

    byte showCurrentByte() override
    {
        throw AIoException(A_EXPOINT);
    }

    void skipCurrentByte() override
    {
        throw AIoException(A_EXPOINT);
    }

    int64 getBytesAvailableCount() override
    {
        return 0;
    }

private:
// Deleted
    ANullStreamSource(const ANullStreamSource&) = delete;
    ANullStreamSource& operator = (const ANullStreamSource&) = delete;
};
//=============================================================================
#endif // AGE_HELPERS_STREAMS_ANULLSTREAMSOURCE_H_INCLUDED
