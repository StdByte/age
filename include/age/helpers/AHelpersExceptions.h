/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_HELPERS_AHELPERSEXCEPTIONS_H_INCLUDED
#define AGE_HELPERS_AHELPERSEXCEPTIONS_H_INCLUDED

#include "age/core/exceptions/AException.h"

//=============================================================================
class AIoException : public AException
{
public:
// Interface
    template<class F>
    AIoException(F&& func, int32 line) :
        AException("IO error", std::forward<F>(func), line)
    {
    }
};
//=============================================================================
#endif // AGE_HELPERS_AHELPERSEXCEPTIONS_H_INCLUDED
