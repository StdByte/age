/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_AUNICODE_H_INCLUDED
#define AGE_CORE_AUNICODE_H_INCLUDED

#include <codecvt>
#include <locale>
#include <string>

//=============================================================================
class AUnicode
{
public:
// Interface
    template <class WChar = char16_t>
    static std::basic_string<WChar> toUtf16(const std::string& utf8str)
    {
        Converter<std::codecvt_utf8_utf16<WChar>> converter;
        return converter.from_bytes(utf8str);
    }

    template <class WChar = char32_t>
    static std::basic_string<WChar> toUtf32(const std::string& utf8str)
    {
        Converter<std::codecvt_utf8<WChar>> converter;
        return converter.from_bytes(utf8str);
    }

    template <class WChar>
    static std::string fromUtf16(const std::basic_string<WChar>& utf16str)
    {
        Converter<std::codecvt_utf8_utf16<WChar>> converter;
        return converter.to_bytes(utf16str);
    }

    template <class WChar>
    static std::string fromUtf32(const std::basic_string<WChar>& utf32str)
    {
        Converter<std::codecvt_utf8<WChar>> converter;
        return converter.to_bytes(utf32str);
    }

private:
// Helpers
    template<class C>
    using Converter = std::wstring_convert<C, typename C::intern_type>;
};
//=============================================================================
#endif // AGE_CORE_AUNICODE_H_INCLUDED
