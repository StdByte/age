/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_SYSTEM_AFILE_H_INCLUDED
#define AGE_CORE_SYSTEM_AFILE_H_INCLUDED

#include <memory>
#include <string>

#include "age/core/types/AAliases.h"

//=============================================================================
class AFile
{
public:
// Helpers
    enum FileWhence
    {
        FW_BEGIN,
        FW_CURRENT,
        FW_END
    };

    enum OpenMode
    {
        OM_READ       = 0b0001,
        OM_WRITE      = 0b0010,
        OM_READ_WRITE = OM_READ | OM_WRITE,
        OM_TRUNC      = 0b0100,
        OM_ATEND      = 0b1000
    };

public:
// Interface
    static bool isExists(const std::string& path);
    static void removeIfExists(const std::string& path);
    static void rename(const std::string& oldPath, const std::string& newPath);
    static void copy(const std::string& oldPath, const std::string& newPath);

    static int64 getSize(const std::string& path);

public:
// Interface
             AFile();
    explicit AFile(const std::string& path, int32 mode = OM_READ_WRITE);
             AFile(AFile&& file) noexcept;
            ~AFile();

    AFile& operator = (AFile&& file) noexcept;

    void open(const std::string& path, int32 mode = OM_READ_WRITE);
    void close();
    bool isOpen() const;

    const std::string& getPath() const;

    int64 setPos(int64 offset, FileWhence whence = FW_BEGIN);
    int64 getPos() const;

    int64 write(const void* data, int64 len);
    int64 write(const std::string& str)
    {
        return write(str.c_str(), str.size());
    }

    int64 read(void* data, int64 len);

private:
// Data
    std::unique_ptr<class ANativeFile> impl_;

private:
// Deleted
    AFile(const AFile&) = delete;
    AFile& operator = (const AFile&) = delete;
};
//=============================================================================
#endif // AGE_CORE_SYSTEM_AFILE_H_INCLUDED
