/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_SYSTEM_AWINDOW_H_INCLUDED
#define AGE_CORE_SYSTEM_AWINDOW_H_INCLUDED

#include <memory>
#include <string>

#include "age/core/types/AAliases.h"

class AEvent;

//=============================================================================
class AWindow
{
public:
// Interface
    static AWindow& getInstance();

public:
// Interface
    void               setTitle(const std::string& title);
    const std::string& getTitle() const;

    void setCursorVisible(bool isVisible);
    bool isCursorVisible() const;

    void setFullscreen();
    bool isFullscreen() const;

    void         setSize(const ASize& size);
    void         setSize(float x, float y) { setSize(ASize(x, y)); }
    const ASize& getSize() const;

    const APos& getCursorPos() const;

    void create();
    void update();
    void destroy();

    bool pollEvent(AEvent* event);

private:
// Data
    std::unique_ptr<class ANativeWindow> impl_;

private:
// Deleted
    AWindow(const AWindow&) = delete;
    AWindow& operator = (const AWindow&) = delete;

private:
// Constructor
    AWindow();
};
//=============================================================================
#endif // AGE_CORE_SYSTEM_AWINDOW_H_INCLUDED
