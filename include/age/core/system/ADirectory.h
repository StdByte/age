/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_SYSTEM_ADIRECTORY_H_INCLUDED
#define AGE_CORE_SYSTEM_ADIRECTORY_H_INCLUDED

#include <memory>
#include <string>
#include <list>

#include "age/core/types/AAliases.h"

//=============================================================================
class ADirectory
{
public:
// Helpers
    enum EntryType
    {
        ET_NONE = 0b00,
        ET_DIR  = 0b01,
        ET_FILE = 0b10,
        ET_ALL  = ET_DIR | ET_FILE
    };

    struct Entry
    {
        std::string name;
        EntryType   type = ET_NONE;
    };

    using Entries = std::list<Entry>;

public:
// Interface
    static bool isExists(const std::string& path);
    static void create(const std::string& path, bool isMayExists = true);
    static void createRecursively(const std::string& path);
    static void rename(const std::string& oldPath, const std::string& newPath);
    static void removeIfExists(const std::string& path);
    static void removeContentRecursively(const std::string& path);
    static void getContent(const std::string& path, Entries* entries,
        int32 types = ET_ALL);

    static int64 getContentSizeRecursively(const std::string& path);

    // Compute and get prefer directory
    static void       setMeta(const std::string& org, const std::string& app);
    static ADirectory getPreferDir();

public:
// Interface
             ADirectory();
    explicit ADirectory(const std::string& path);
             ADirectory(const ADirectory& dir);
             ADirectory(ADirectory&& dir) noexcept;
            ~ADirectory();

    ADirectory& operator = (const ADirectory& dir);
    ADirectory& operator = (ADirectory&& dir) noexcept;

    bool operator == (const ADirectory& dir) const;
    bool operator != (const ADirectory& dir) const
    {
        return !(operator==(dir));
    }

    void               setPath(const std::string& path);
    const std::string& getPath() const; // With '/' at end

    void goTo(const std::string& subPath);
    void goUp();

    std::string getEntryPath(const std::string& name, bool isDir) const;
    std::string getEntryPath(const Entry& entry) const
    {
        return getEntryPath(entry.name, (entry.type == ET_DIR));
    }

    bool isExists() const;
    void create(bool isMayExists = true);
    void createRecursively();
    void removeIfExists();
    void removeContentRecursively();
    void rename(const std::string& newPath);
    void getContent(Entries* entries, int32 types = ET_ALL) const;

    bool isValid() const;
    void clear();

private:
// Data
    std::unique_ptr<class ANativeDirectory> impl_;
};
//=============================================================================
#endif // AGE_CORE_SYSTEM_ADIRECTORY_H_INCLUDED
