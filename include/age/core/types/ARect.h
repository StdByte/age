/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_TYPES_ARECT_H_INCLUDED
#define AGE_CORE_TYPES_ARECT_H_INCLUDED

#include "age/core/types/AAliases.h"

//=============================================================================
struct ARect
{
    APos  pos;
    ASize size;

    ARect()                                                           {}
    ARect(const APos& pos, const ASize& size) : pos(pos),  size(size) {}
    ARect(float posX, float posY, float sizeX, float sizeY) :
        pos(posX, posY),
        size(sizeX, sizeY)
    {}

    bool operator == (const ARect& rect) const
    {
        return pos == rect.pos && size == rect.size;
    }

    bool operator != (const ARect& rect) const
    {
        return !operator==(rect);
    }
};
//=============================================================================
#endif // AGE_CORE_TYPES_ARECT_H_INCLUDED
