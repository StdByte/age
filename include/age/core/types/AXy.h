/*
Copyright (C) 2018 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_TYPES_AXY_H_INCLUDED
#define AGE_CORE_TYPES_AXY_H_INCLUDED

//=============================================================================
struct AXy
{
    float x;
    float y;

    AXy()                 {       x = 0;       y = 0; }
    AXy(float x, float y) { this->x = x; this->y = y; }

    bool operator == (const AXy& xy) const
    {
        return (x == xy.x) && (y == xy.y);
    }

    bool operator != (const AXy& xy) const
    {
        return !operator==(xy);
    }

    AXy& operator += (const AXy& xy)
    {
        x += xy.x;
        y += xy.y;
        return *this;
    }

    AXy& operator -= (const AXy& xy)
    {
        x -= xy.x;
        y -= xy.y;
        return *this;
    }

    AXy& operator *= (float value)
    {
        x *= value;
        y *= value;
        return *this;
    }

    AXy& operator /= (float value)
    {
        x /= value;
        y /= value;
        return *this;
    }
};
//=============================================================================
inline
AXy
operator + (const AXy& left, const AXy& right)
{
    return AXy(left.x + right.x, left.y + right.y);
}
//=============================================================================
inline
AXy
operator - (const AXy& left, const AXy& right)
{
    return AXy(left.x - right.x, left.y - right.y);
}
//=============================================================================
inline
AXy
operator * (const AXy& xy, float value)
{
    return AXy(xy.x * value, xy.y * value);
}
//=============================================================================
inline
AXy
operator / (const AXy& xy, float value)
{
    return AXy(xy.x / value, xy.y / value);
}
//=============================================================================
#endif // AGE_CORE_TYPES_AXY_H_INCLUDED
