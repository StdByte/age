/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_TYPES_AALIASES_H_INCLUDED
#define AGE_CORE_TYPES_AALIASES_H_INCLUDED

#include <cstdint>

#include "age/core/types/AXy.h"

//=============================================================================
// Short aliases
using  byte  = std::uint8_t;
using  int8  = std::int8_t;
using uint8  = std::uint8_t;
using  int16 = std::int16_t;
using uint16 = std::uint16_t;
using  int32 = std::int32_t;
using uint32 = std::uint32_t;
using  int64 = std::int64_t;
using uint64 = std::uint64_t;
//=============================================================================
using AVector = AXy;
using ASize   = AXy;
using APos    = AXy;
//=============================================================================
#endif // AGE_CORE_TYPES_AALIASES_H_INCLUDED
