/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_EXCEPTIONS_ALOGICEXCEPTIONS_H_INCLUDED
#define AGE_CORE_EXCEPTIONS_ALOGICEXCEPTIONS_H_INCLUDED

#include "age/core/exceptions/AException.h"

//=============================================================================
class ALogicException : public AException
{
public:
// Interface
    template<class W, class F>
    ALogicException(W&& what, F&& func, int32 line) :
        AException(std::forward<W>(what), std::forward<F>(func), line)
    {
    }
};
//=============================================================================
class ANullptrException : public ALogicException
{
public:
// Interface
    template<class F>
    ANullptrException(F&& func, int32 line) :
        ALogicException("Nullptr", std::forward<F>(func), line) {}
};
//=============================================================================
class ABadParametersException : public ALogicException
{
public:
// Interface
    template<class F>
    ABadParametersException(F&& func, int32 line) :
        ALogicException("Bad parameters", std::forward<F>(func), line) {}
};
//=============================================================================
class ADivisionByZeroException : public ALogicException
{
public:
// Interface
    template<class F>
    ADivisionByZeroException(F&& func, int32 line) :
        ALogicException("Division by zero", std::forward<F>(func), line) {}
};
//=============================================================================
class AOutOfRangeException : public ALogicException
{
public:
// Interface
    template<class F>
    AOutOfRangeException(F&& func, int32 line) :
        ALogicException("Out of range", std::forward<F>(func), line) {}
};
//=============================================================================
class AUndefinedBehaviorException : public ALogicException
{
public:
// Interface
    template<class F>
    AUndefinedBehaviorException(F&& func, int32 line) :
        ALogicException("Undefined behavior", std::forward<F>(func), line) {}
};
//=============================================================================
class AUnresolvedOperationException : public ALogicException
{
public:
// Interface
    template<class F>
    AUnresolvedOperationException(F&& func, int32 line) :
        ALogicException("Unresolved operation", std::forward<F>(func), line) {}
};
//=============================================================================
#endif // AGE_CORE_EXCEPTIONS_ASTANDARDEXCEPTIONS_H_INCLUDED
