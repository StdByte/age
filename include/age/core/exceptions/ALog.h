/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_EXCEPTIONS_ALOG_H_INCLUDED
#define AGE_CORE_EXCEPTIONS_ALOG_H_INCLUDED

#include <exception>
#include <string>

//=============================================================================
class ALog
{
public:
// Interface
    template<class P>
    static void setLogPath(P&& path)
    {
        logPath_ = std::forward<P>(path);
    }

    static void write(const std::string& what) noexcept
    {
        write(what.c_str());
    }

    // Threadsafe
    static void write(const char* what) noexcept;
    static void writeException(const std::exception& ex) noexcept;

private:
// Data
    static std::string logPath_;
};
//=============================================================================
#endif // AGE_CORE_EXCEPTIONS_ALOG_H_INCLUDED
