/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_EXCEPTIONS_ASYSTEMEXCEPTIONS_H_INCLUDED
#define AGE_CORE_EXCEPTIONS_ASYSTEMEXCEPTIONS_H_INCLUDED

#include "age/core/exceptions/AException.h"

//=============================================================================
class ASystemException : public AException
{
public:
// Interface
    template<class W, class F>
    ASystemException(W&& what, F&& func, int32 line) :
        AException(std::forward<W>(what), std::forward<F>(func), line)
    {
    }
};
//=============================================================================
class ADirectoryException : public ASystemException
{
public:
// Interface
    template<class W, class F>
    ADirectoryException(W&& what, F&& func, int32 line) :
        ASystemException(std::forward<W>(what), std::forward<F>(func), line)
    {
    }
};
//=============================================================================
class AFileException : public ASystemException
{
public:
// Interface
    template<class W, class F>
    AFileException(W&& what, F&& func, int32 line) :
        ASystemException(std::forward<W>(what), std::forward<F>(func), line)
    {
    }
};
//=============================================================================
class AWindowException : public ASystemException
{
public:
// Interface
    template<class W, class F>
    AWindowException(W&& what, F&& func, int32 line) :
        ASystemException(std::forward<W>(what), std::forward<F>(func), line)
    {
    }
};
//=============================================================================
class AFailedCreateDirException : public ADirectoryException
{
public:
// Interface
    template<class P, class F>
    AFailedCreateDirException(P&& path, F&& func, int32 line) :
        ADirectoryException("Failed to create directory",
            std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedRenameDirException : public ADirectoryException
{
public:
// Interface
    template<class O, class N, class F>
    AFailedRenameDirException(O&& oldPath, N&& newPath, F&& func, int32 line) :
        ADirectoryException("Failed to rename directory",
            std::forward<F>(func), line)
    {
        addDetail("oldPath", std::forward<O>(oldPath));
        addDetail("newPath", std::forward<N>(newPath));
    }
};
//=============================================================================
class AFailedRemoveDirException : public ADirectoryException
{
public:
// Interface
    template<class P, class F>
    AFailedRemoveDirException(P&& path, F&& func, int32 line) :
        ADirectoryException("Failed to remove directory",
            std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedOpenDirException : public ADirectoryException
{
public:
// Interface
    template<class P, class F>
    AFailedOpenDirException(P&& path, F&& func, int32 line) :
        ADirectoryException("Failed to open directory",
            std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedGetSystemPathException : public ADirectoryException
{
public:
// Interface
    template<class F>
    AFailedGetSystemPathException(F&& func, int32 line) :
        ADirectoryException("Failed to get system path",
            std::forward<F>(func), line)
    {
    }
};
//=============================================================================
class AFailedRemoveFileException : public AFileException
{
public:
// Interface
    template<class P, class F>
    AFailedRemoveFileException(P&& path, F&& func, int32 line) :
        AFileException("Failed to remove file", std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedRenameFileException : public AFileException
{
public:
// Interface
    template<class O, class N, class F>
    AFailedRenameFileException(O&& oldPath, N&& newPath, F&& func,
            int32 line) :
        AFileException("Failed to rename file", std::forward<F>(func), line)
    {
        addDetail("oldPath", std::forward<O>(oldPath));
        addDetail("newPath", std::forward<N>(newPath));
    }
};
//=============================================================================
class AFailedOpenFileException : public AFileException
{
public:
// Interface
    template<class P, class F>
    AFailedOpenFileException(P&& path, F&& func, int32 line) :
        AFileException("Failed to open file", std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedMoveFileCursorException : public AFileException
{
public:
// Interface
    template<class P, class F>
    AFailedMoveFileCursorException(P&& path, F&& func, int32 line) :
        AFileException("Failed to change file cursor",
            std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedWriteFileException : public AFileException
{
public:
// Interface
    template<class P, class F>
    AFailedWriteFileException(P&& path, F&& func, int32 line) :
        AFileException("Failed to write file", std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
class AFailedReadFileException : public AFileException
{
public:
// Interface
    template<class P, class F>
    AFailedReadFileException(P&& path, F&& func, int32 line) :
        AFileException("Failed to read file", std::forward<F>(func), line)
    {
        addDetail("path", std::forward<P>(path));
    }
};
//=============================================================================
#endif // AGE_CORE_EXCEPTIONS_ASYSTEMEXCEPTIONS_H_INCLUDED
