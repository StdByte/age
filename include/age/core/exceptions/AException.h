/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AGE_CORE_EXCEPTIONS_AEXCEPTION_H_INCLUDED
#define AGE_CORE_EXCEPTIONS_AEXCEPTION_H_INCLUDED

#include <exception>
#include <string>
#include <list>

#include "age/core/types/AAliases.h"

//=============================================================================
class AException : public std::exception
{
public:
// Helpers
    struct Detail
    {
        std::string title;
        std::string description;

        Detail() = default;

        template<class T, class D>
        Detail(T&& title, D&& description) :
            title(std::forward<T>(title)),
            description(std::forward<D>(description))
        {
        }
    };

    using Details = std::list<Detail>;

public:
// Interface
    template<class W, class F>
    AException(W&& what, F&& func, int32 line) :
        what_(std::forward<W>(what)),
        func_(std::forward<F>(func))
    {
        line_ = line;
    }

    const char*        what() const noexcept override { return what_.c_str(); }
    const std::string& getWhat() const noexcept       { return what_;         }
    const std::string& getFunc() const noexcept       { return func_;         }
    int32              getLine() const noexcept       { return line_;         }
    const Details&     getDetails() const noexcept    { return details_;      }

protected:
// Methods
    template<class T, class D>
    void addDetail(T&& title, D&& description)
    {
        details_.emplace_back(std::forward<T>(title),
                              std::forward<D>(description));
    }

private:
// Data
    std::string what_;
    std::string func_;
    Details     details_;
    int32       line_;
};
//=============================================================================
#define A_EXPOINT __PRETTY_FUNCTION__, __LINE__
//=============================================================================
#endif // AGE_CORE_EXCEPTIONS_AEXCEPTION_H_INCLUDED
