/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <mutex>
#include <ctime>
#include <string>
#include <exception>

#include "age/core/exceptions/ALog.h"
#include "age/core/system/AFile.h"
#include "age/core/types/AAliases.h"

#include "age/core/exceptions/AException.h"

std::string ALog::logPath_("log");
//=============================================================================
void
ALog::write(const char* what)
    noexcept
{
    if(!what)
        return;

    static bool isCallStacked = false;

    try
    {
        static std::mutex           lock;
        std::lock_guard<std::mutex> guard(lock);

        // Protection from a recursive call
        if(isCallStacked)
            return;
        isCallStacked = true;

        std::time_t secs = std::time(nullptr);
        std::tm*    time = std::localtime(&secs);

        const char* dateStrFormat = "%Y-%m-%d";
        const int32 dateStrLen    = 11;

        const char* timeStrFormat = "%H:%M:%S";
        const int32 timeStrLen    = 9;

        char dateStr[dateStrLen];
        char timeStr[timeStrLen];

        strftime(dateStr, dateStrLen, dateStrFormat, time);
        strftime(timeStr, timeStrLen, timeStrFormat, time);

        AFile logFile;
        logFile.open(logPath_, AFile::OM_WRITE | AFile::OM_ATEND);

        std::string logMessage;

        if(logFile.getPos() > 0)
            logMessage += '\n';

        logMessage += '[';
        logMessage += dateStr;
        logMessage += ' ';
        logMessage += timeStr;
        logMessage += ']';
        logMessage += what;

        logFile.write(logMessage);
    }
    catch(...)
    {
    }

    isCallStacked = false;
}
//=============================================================================
void
ALog::writeException(const std::exception& ex)
    noexcept
{
    try
    {
        const AException* aex = dynamic_cast<const AException*>(&ex);
        std::string message;

        if(aex)
        {
            int32 line = aex->getLine();

            message += '[';
            message += aex->getFunc();
            message += ':';
            message += std::to_string(line);
            message += ']';
            message += aex->getWhat();

            const AException::Details& details = aex->getDetails();

            for(const AException::Detail& detail : details)
            {
                message += '[';
                message += detail.title;
                message += ':';
                message += detail.description;
                message += ']';
            }

        } else
        {
            message += ex.what();
        }

        write(message);
    }
    catch(...)
    {
    }
}
//=============================================================================
