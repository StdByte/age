/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <algorithm>

#include "age/core/system/AWindow.h"
#include "age/core/system/AEvent.h"

#include "core/system/ANativeWindow.h"

//=============================================================================
AWindow&
AWindow::getInstance()
{
    static AWindow window;
    return window;
}
//=============================================================================
void
AWindow::setTitle(const std::string& title)
{
    impl_->setTitle(title);
}
//=============================================================================
const std::string&
AWindow::getTitle()
    const
{
    return impl_->getTitle();
}
//=============================================================================
void
AWindow::setCursorVisible(bool isVisible)
{
    impl_->setCursorVisible(isVisible);
}
//=============================================================================
bool
AWindow::isCursorVisible()
    const
{
    return impl_->isCursorVisible();
}
//=============================================================================
void
AWindow::setFullscreen()
{
    impl_->setFullscreen(true);
}
//=============================================================================
bool
AWindow::isFullscreen()
    const
{
    return impl_->isFullscreen();
}
//=============================================================================
void
AWindow::setSize(const ASize& size)
{
    impl_->setSize(size);
}
//=============================================================================
const ASize&
AWindow::getSize()
    const
{
    return impl_->getSize();
}
//=============================================================================
const APos&
AWindow::getCursorPos()
    const
{
    return impl_->getCursorPos();
}
//=============================================================================
void
AWindow::create()
{
    impl_->create();
}
//=============================================================================
void
AWindow::update()
{
    impl_->update();
}
//=============================================================================
void
AWindow::destroy()
{
    impl_->destroy();
}
//=============================================================================
bool
AWindow::pollEvent(AEvent* event)
{
    return impl_->pollEvent(event);
}
//=============================================================================
AWindow::AWindow() :
    impl_(new ANativeWindow())
{
}
//=============================================================================
