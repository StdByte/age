/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "age/core/system/ADirectory.h"
#include "age/core/system/AFile.h"

#include "age/core/exceptions/ALogicExceptions.h"

#include "core/system/ANativeDirectory.h"

//=============================================================================
bool
ADirectory::isExists(const std::string& path)
{
    return ANativeDirectory::isExists(path);
}
//=============================================================================
void
ADirectory::create(const std::string& path, bool isMayExists)
{
    ANativeDirectory::create(path, isMayExists);
}
//=============================================================================
void
ADirectory::createRecursively(const std::string& path)
{
    ANativeDirectory::createRecursively(path);
}
//=============================================================================
void
ADirectory::rename(const std::string& oldPath, const std::string& newPath)
{
    ANativeDirectory::rename(oldPath, newPath);
}
//=============================================================================
void
ADirectory::removeIfExists(const std::string& path)
{
    ANativeDirectory::removeIfExists(path);
}
//=============================================================================
void
ADirectory::removeContentRecursively(const std::string& path)
{
    bool    isPathCorrected = (path.back() == '/');
    Entries entries;

    getContent(path, &entries);

    for(const Entry& entry : entries)
    {
        std::string elementPath = path;
        if(!isPathCorrected)
            elementPath += '/';
        elementPath += entry.name;

        if(entry.type == ET_DIR)
        {
            removeContentRecursively(elementPath);
            removeIfExists(elementPath);
            continue;
        }

        AFile::removeIfExists(elementPath);
    }
}
//=============================================================================
void
ADirectory::getContent(const std::string& path, Entries* entries, int32 types)
{
    ANativeDirectory::getContent(path, entries, types);
}
//=============================================================================
int64
ADirectory::getContentSizeRecursively(const std::string& path)
{
    int64   result          = 0;
    bool    isPathCorrected = (path.back() == '/');
    Entries entries;

    getContent(path, &entries);

    for(const Entry& entry : entries)
    {
        std::string elementPath = path;
        if(!isPathCorrected)
            elementPath += '/';
        elementPath += entry.name;

        if(entry.type == ET_DIR)
            result += getContentSizeRecursively(elementPath);
        else result += AFile::getSize(elementPath);
    }

    return result;
}
//=============================================================================
void
ADirectory::setMeta(const std::string& org, const std::string& app)
{
    ANativeDirectory::setMeta(org, app);
}
//=============================================================================
ADirectory
ADirectory::getPreferDir()
{
    ADirectory result;
    ANativeDirectory::getPreferDir(result.impl_.get());
    return result;
}
//=============================================================================
ADirectory::ADirectory() :
    impl_(new ANativeDirectory())
{
}
//=============================================================================
ADirectory::ADirectory(const std::string& path) :
    ADirectory()
{
    impl_->setPath(path);
}
//=============================================================================
ADirectory::ADirectory(const ADirectory& dir) :
    impl_(new ANativeDirectory(*dir.impl_))
{
}
//=============================================================================
ADirectory::ADirectory(ADirectory&& dir)
    noexcept :
    ADirectory()
{
    std::swap(impl_, dir.impl_);
}
//=============================================================================
ADirectory::~ADirectory()
{
}
//=============================================================================
ADirectory&
ADirectory::operator = (const ADirectory& dir)
{
    if(this == &dir)
        return *this;

    *impl_ = *dir.impl_;

    return *this;
}
//=============================================================================
ADirectory&
ADirectory::operator = (ADirectory&& dir)
    noexcept
{
    if(this == &dir)
        return *this;

    std::swap(impl_, dir.impl_);

    return *this;
}
//=============================================================================
bool
ADirectory::operator == (const ADirectory& dir)
    const
{
    return (*impl_ == *dir.impl_);
}
//=============================================================================
void
ADirectory::setPath(const std::string& path)
{
    impl_->setPath(path);
}
//=============================================================================
const std::string&
ADirectory::getPath()
    const
{
    return impl_->getPath();
}
//=============================================================================
void
ADirectory::goTo(const std::string& subPath)
{
    impl_->goTo(subPath);
}
//=============================================================================
void
ADirectory::goUp()
{
    impl_->goUp();
}
//=============================================================================
std::string
ADirectory::getEntryPath(const std::string& name, bool isDir)
    const
{
    return impl_->getEntryPath(name, isDir);
}
//=============================================================================
bool
ADirectory::isExists()
    const
{
    return impl_->isExists();
}
//=============================================================================
void
ADirectory::create(bool isMayExists)
{
    impl_->create(isMayExists);
}
//=============================================================================
void
ADirectory::createRecursively()
{
    impl_->createRecursively();
}
//=============================================================================
void
ADirectory::rename(const std::string& newPath)
{
    impl_->rename(newPath);
}
//=============================================================================
void
ADirectory::removeIfExists()
{
    impl_->removeIfExists();
}
//=============================================================================
void
ADirectory::removeContentRecursively()
{
    const std::string& path = impl_->getPath();
    removeContentRecursively(path);
}
//=============================================================================
void
ADirectory::getContent(Entries* entries, int32 types)
    const
{
    impl_->getContent(entries, types);
}
//=============================================================================
bool
ADirectory::isValid()
    const
{
    return impl_->isValid();
}
//=============================================================================
void
ADirectory::clear()
{
    impl_->clear();
}
//=============================================================================
