/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <algorithm>

#include "age/core/system/AFile.h"

#include "core/system/ANativeFile.h"

//=============================================================================
bool
AFile::isExists(const std::string& path)
{
    return ANativeFile::isExists(path);
}
//=============================================================================
void
AFile::removeIfExists(const std::string& path)
{
    ANativeFile::removeIfExists(path);
}
//=============================================================================
void
AFile::rename(const std::string& oldPath, const std::string& newPath)
{
    ANativeFile::rename(oldPath, newPath);
}
//=============================================================================
void
AFile::copy(const std::string& oldPath, const std::string& newPath)
{
    const int64 BUFFER_SIZE = 1024;

    AFile reader(oldPath, AFile::OM_READ);
    AFile writer(newPath, AFile::OM_WRITE | AFile::OM_TRUNC);

    std::unique_ptr<char[]> buffer(new char[BUFFER_SIZE]);

    int64 readed = 0;

    while(true)
    {
        readed = reader.read(buffer.get(), BUFFER_SIZE);

        if(readed > 0)
            writer.write(buffer.get(), readed);
        else break;
    }
}
//=============================================================================
int64
AFile::getSize(const std::string& path)
{
    ANativeFile file;
    file.open(path, OM_READ);
    return file.setPos(0, FW_END);
}
//=============================================================================
AFile::AFile() :
    impl_(new ANativeFile())
{
}
//=============================================================================
AFile::AFile(const std::string& path, int32 mode) :
    AFile()
{
    impl_->open(path, mode);
}
//=============================================================================
AFile::AFile(AFile&& file)
    noexcept :
    AFile()
{
    std::swap(impl_, file.impl_);
}
//=============================================================================
AFile::~AFile()
{
}
//=============================================================================
AFile&
AFile::operator = (AFile&& file)
    noexcept
{
    if(this == &file)
        return *this;

    std::swap(impl_, file.impl_);

    return *this;
}
//=============================================================================
void
AFile::open(const std::string& path, int32 mode)
{
    impl_->open(path, mode);
}
//=============================================================================
void
AFile::close()
{
    impl_->close();
}
//=============================================================================
bool
AFile::isOpen()
    const
{
    return impl_->isOpen();
}
//=============================================================================
const std::string&
AFile::getPath()
    const
{
    return impl_->getPath();
}
//=============================================================================
int64
AFile::setPos(int64 offset, FileWhence whence)
{
    return impl_->setPos(offset, whence);
}
//=============================================================================
int64
AFile::getPos()
    const
{
    return impl_->getPos();
}
//=============================================================================
int64
AFile::write(const void* data, int64 len)
{
    return impl_->write(data, len);
}
//=============================================================================
int64
AFile::read(void* data, int64 len)
{
    return impl_->read(data, len);
}
//=============================================================================
