/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <algorithm>

#include "age/core/exceptions/ALogicExceptions.h"

#include "age/helpers/AHelpersExceptions.h"

#include "helpers/streams/AFileStreamSourceReader.h"
#include "helpers/streams/AFileStreamSourceData.h"

//=============================================================================
AFileStreamSourceReader::AFileStreamSourceReader(AFileStreamSourceData* data)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    data_       = data;
    bufferPos_  = 0;
    bufferLen_  = 0;
}
//=============================================================================
void
AFileStreamSourceReader::initialize()
{
    bufferPos_  = 0;
    bufferLen_  = 0;
}
//=============================================================================
void
AFileStreamSourceReader::release()
{
    try
    {
        data_->realPos = data_->file.setPos(data_->virtualPos,
            AFile::FW_BEGIN);
    }
    catch(...)
    {
        data_->virtualPos = data_->realPos;
        throw;
    }
}
//=============================================================================
void
AFileStreamSourceReader::read(void* data, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    if(len > (data_->fileSize - data_->virtualPos))
        throw AIoException(A_EXPOINT);

    byte* outData = reinterpret_cast<byte*>(data);
    int64 outLen  = len;

    int64 readed = readFromBuffer(outData, data_->virtualPos, outLen);

    data_->virtualPos += readed;

    if(readed != len)
    {
        outData += readed;
        outLen  -= readed;

        if(outLen > (AFileStreamSourceData::BUFFER_SIZE / 2))
        {
            readFromFile(outData, data_->virtualPos, outLen);
            data_->virtualPos += outLen;
        }
        else
        {
            fillBuffer(data_->virtualPos);
            read(outData, outLen);
        }
    }
}
//=============================================================================
byte
AFileStreamSourceReader::showCurrentByte()
{
    byte  data = 0;
    int64 len  = sizeof(byte);

    read(&data, len);

    data_->virtualPos -= len;

    return data;
}
//=============================================================================
void
AFileStreamSourceReader::skipCurrentByte()
{
    if(data_->virtualPos == data_->fileSize)
        throw AIoException(A_EXPOINT);

    ++data_->virtualPos;
}
//=============================================================================
void
AFileStreamSourceReader::fillBuffer(int64 pos)
{
    if(pos > data_->fileSize)
        throw AIoException(A_EXPOINT);

    int64 len = std::min(AFileStreamSourceData::BUFFER_SIZE,
                         data_->fileSize - pos);
    if(len == 0)
        return;

    readFromFile(data_->buffer.get(), pos, len);

    bufferPos_ = pos;
    bufferLen_ = len;
}
//=============================================================================
void
AFileStreamSourceReader::readFromFile(byte* data, int64 pos, int64 len)
{
    if(data_->realPos != pos)
        data_->realPos = data_->file.setPos(pos, AFile::FW_BEGIN);

    int64 readed = data_->file.read(data, len);

    data_->realPos += readed;

    if(readed != len)
        throw AIoException(A_EXPOINT);
}
//=============================================================================
int64
AFileStreamSourceReader::readFromBuffer(byte* data, int64 pos, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    if(pos < bufferPos_)
        return 0;

    int64 readCount = std::min((bufferPos_ + bufferLen_ - pos), len);
    if(readCount <= 0)
        return 0;

    byte* buffer = data_->buffer.get() + (pos - bufferPos_);
    std::copy(buffer, buffer + readCount, data);

    return readCount;
}
//=============================================================================
