/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <algorithm>
#include <exception>

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/core/exceptions/ALog.h"

#include "age/helpers/streams/AFileStreamSource.h"
#include "age/helpers/AHelpersExceptions.h"

#include "helpers/streams/AFileStreamSourceData.h"
#include "helpers/streams/AFileStreamSourceReader.h"
#include "helpers/streams/AFileStreamSourceWriter.h"

//=============================================================================
AFileStreamSource::AFileStreamSource(const std::string& path, int32 mode)
{
    mode_   = SM_NONE;
    data_   = std::make_unique<AFileStreamSourceData>(path, mode);
    reader_ = std::make_unique<AFileStreamSourceReader>(data_.get());
    writer_ = std::make_unique<AFileStreamSourceWriter>(data_.get());
}
//=============================================================================
AFileStreamSource::~AFileStreamSource()
{
    try
    {
        applyMode(SM_NONE);
    }
    catch(const std::exception& ex)
    {
        ALog::writeException(ex);
    }
}
//=============================================================================
bool
AFileStreamSource::isEof()
    const
{
    return (data_->virtualPos >= data_->fileSize);
}
//=============================================================================
int64
AFileStreamSource::setPos(int64 offset, StreamWhence whence)
{
    applyMode(SM_NONE);

    int64 beginOffset = (whence == SW_BEGIN)   ? offset
                      : (whence == SW_CURRENT) ? data_->virtualPos + offset
                      : data_->fileSize + offset;

    if(beginOffset > data_->fileSize)
        throw AIoException(A_EXPOINT);

    data_->file.setPos(beginOffset, AFile::FW_BEGIN);
    data_->virtualPos = beginOffset;
    data_->realPos    = beginOffset;

    return beginOffset;
}
//=============================================================================
int64
AFileStreamSource::getPos()
    const
{
    return data_->virtualPos;
}
//=============================================================================
void
AFileStreamSource::write(const void* data, int64 len)
{
    applyMode(SM_WRITE);
    writer_->write(data, len);
}
//=============================================================================
void
AFileStreamSource::read(void* data, int64 len)
{
    applyMode(SM_READ);
    reader_->read(data, len);
}
//=============================================================================
byte
AFileStreamSource::showCurrentByte()
{
    applyMode(SM_READ);
    return reader_->showCurrentByte();
}
//=============================================================================
void
AFileStreamSource::skipCurrentByte()
{
    if(mode_ == SM_READ)
    {
        reader_->skipCurrentByte();
        return;
    }

    applyMode(SM_NONE);

    if(isEof())
        throw AIoException(A_EXPOINT);

    int64 newPos = data_->virtualPos + 1;

    data_->file.setPos(newPos, AFile::FW_BEGIN);
    data_->realPos    = newPos;
    data_->virtualPos = newPos;
}
//=============================================================================
int64
AFileStreamSource::getBytesAvailableCount()
{
    return std::max<int64>(data_->fileSize - data_->virtualPos, 0);
}
//=============================================================================
void
AFileStreamSource::applyMode(SourceMode mode)
{
    if(mode_ == mode)
        return;

    switch(mode_)
    {
        case SM_NONE:  break;
        case SM_READ:  reader_->release(); break;
        case SM_WRITE: writer_->release(); break;
    }

    mode_ = mode;

    switch(mode_)
    {
        case SM_NONE:  break;
        case SM_READ:  reader_->initialize(); break;
        case SM_WRITE: writer_->initialize(); break;
    }
}
//=============================================================================
