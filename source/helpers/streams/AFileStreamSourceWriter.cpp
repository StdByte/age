/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <algorithm>

#include "age/core/exceptions/ALogicExceptions.h"

#include "age/helpers/AHelpersExceptions.h"

#include "helpers/streams/AFileStreamSourceWriter.h"
#include "helpers/streams/AFileStreamSourceData.h"

//=============================================================================
AFileStreamSourceWriter::AFileStreamSourceWriter(AFileStreamSourceData* data)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    data_   = data;
    cursor_ = 0;
}
//=============================================================================
void
AFileStreamSourceWriter::initialize()
{
    cursor_ = 0;
}
//=============================================================================
void
AFileStreamSourceWriter::release()
{
    try
    {
        applyBuffer();
    }
    catch(...)
    {
        data_->virtualPos = data_->realPos;
        throw;
    }
}
//=============================================================================
void
AFileStreamSourceWriter::write(const void* data, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    try
    {
        if(len > AFileStreamSourceData::BUFFER_SIZE - cursor_)
            applyBuffer();

        if(len > AFileStreamSourceData::BUFFER_SIZE)
            writeToFile(data, len);
        else
        {
            const byte* in  = reinterpret_cast<const byte*>(data);
            byte*       out = data_->buffer.get() + cursor_;

            std::copy(in, in + len, out);

            cursor_ += len;
        }

        data_->virtualPos += len;
    }
    catch(...)
    {
        data_->virtualPos = data_->realPos;
        throw;
    }
}
//=============================================================================
void
AFileStreamSourceWriter::applyBuffer()
{
    int64 len = cursor_;
    cursor_ = 0;

    writeToFile(data_->buffer.get(), len);
}
//=============================================================================
void
AFileStreamSourceWriter::writeToFile(const void* data, int64 len)
{
    int64 written = data_->file.write(data, len);

    data_->realPos   += written;
    data_->fileSize   = std::max(data_->fileSize, data_->realPos);

    if(written != len)
        throw AIoException(A_EXPOINT);
}
//=============================================================================
