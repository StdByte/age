/*
Copyright (C) 2018 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <cctype>
#include <cstring>

#include "age/core/types/AAliases.h"
#include "age/core/exceptions/ALogicExceptions.h"

#include "age/helpers/streams/ATextStream.h"

//=============================================================================
ATextStream&
ATextStream::operator << (const std::string& str)
{
    write(str.c_str(), str.size());
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator << (const char* str)
{
    write(str, std::strlen(str));
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator << (char c)
{
    write(&c, 1);
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator << (bool value)
{
    int flag = value ? 1 : 0;
    return operator<<(flag);
}
//=============================================================================
ATextStream&
ATextStream::operator << (float value)
{
    std::string str = realToString(value);
    return operator<<(str);
}
//=============================================================================
ATextStream&
ATextStream::operator << (double value)
{
    std::string str = realToString(value);
    return operator<<(str);
}
//=============================================================================
ATextStream&
ATextStream::operator >> (std::string& str)
{
    str.clear();
    skipSpaces();
    readWord(&str);
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator >> (char& c)
{
    read(&c, 1);
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator >> (bool& value)
{
    int flag = 0;
    operator>>(flag);
    value = flag;
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator >> (float& value)
{
    std::string buffer;
    operator>>(buffer);
    value = std::stof(buffer);
    return *this;
}
//=============================================================================
ATextStream&
ATextStream::operator >> (double& value)
{
    std::string buffer;
    operator>>(buffer);
    value = std::stod(buffer);
    return *this;
}
//=============================================================================
void
ATextStream::skipSpaces()
{
    int64 bytesCount = getBytesAvailableCount();
    for(int64 i = 0; i < bytesCount; ++i)
    {
        byte b = showCurrentByte();
        if(std::isspace(b)) skipCurrentByte();
        else                break;
    }
}
//=============================================================================
void
ATextStream::readWord(std::string* word)
{
    if(!word)
        throw ANullptrException(A_EXPOINT);

    int64 bytesCount = getBytesAvailableCount();
    for(int64 i = 0; i < bytesCount; ++i)
    {
        byte b = showCurrentByte();
        if(std::isspace(b))
            break;
        else
        {
            *word += b;
            skipCurrentByte();
        }
    }
}
//=============================================================================
