/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AFILESTREAMSOURCEREADER_H_INCLUDED
#define AFILESTREAMSOURCEREADER_H_INCLUDED

#include "age/core/types/AAliases.h"

class AFileStreamSourceData;

//=============================================================================
class AFileStreamSourceReader
{
public:
// Interface
    AFileStreamSourceReader(AFileStreamSourceData* data);

    void initialize();
    void release();

    void read(void* data, int64 len);

    byte showCurrentByte();
    void skipCurrentByte();

private:
// Methods
    void  fillBuffer(int64 pos);
    void  readFromFile(byte* data, int64 pos, int64 len);
    int64 readFromBuffer(byte* data, int64 pos, int64 len);

private:
// Data
    AFileStreamSourceData* data_;
    int64                  bufferPos_;
    int64                  bufferLen_;
};
//=============================================================================
#endif // AFILESTREAMSOURCEREADER_H_INCLUDED
