/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AFILESTREAMSOURCEWRITER_H_INCLUDED
#define AFILESTREAMSOURCEWRITER_H_INCLUDED

#include "age/core/types/AAliases.h"

class AFileStreamSourceData;

//=============================================================================
class AFileStreamSourceWriter
{
public:
// Interface
    AFileStreamSourceWriter(AFileStreamSourceData* data);

    void initialize();
    void release();

    void write(const void* data, int64 len);

private:
// Methods
    void applyBuffer();
    void writeToFile(const void* data, int64 len);

private:
// Data
    AFileStreamSourceData* data_;
    int64                  cursor_;
};
//=============================================================================
#endif // AFILESTREAMSOURCEWRITER_H_INCLUDED
