/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef AFILESTREAMSOURCEDATA_H_INCLUDED
#define AFILESTREAMSOURCEDATA_H_INCLUDED

#include <memory>
#include <string>

#include "age/core/system/AFile.h"
#include "age/core/types/AAliases.h"

//=============================================================================
struct AFileStreamSourceData
{
    static const int64 BUFFER_SIZE = 1024;

    AFile                   file;
    std::unique_ptr<byte[]> buffer;
    int64                   realPos;
    int64                   virtualPos;
    int64                   fileSize;

    AFileStreamSourceData(const std::string& path, int32 mode) :
        file(path, mode)
    {
        buffer     = std::make_unique<byte[]>(BUFFER_SIZE);
        realPos    = file.getPos();
        virtualPos = realPos;
        fileSize   = file.setPos(0, AFile::FW_END);
        file.setPos(realPos, AFile::FW_BEGIN);
    }
};
//=============================================================================
#endif // AFILESTREAMSOURCEDATA_H_INCLUDED
