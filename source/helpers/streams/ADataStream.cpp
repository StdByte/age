/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "age/core/types/AAliases.h"
#include "age/core/types/ARect.h"
#include "age/core/types/AXy.h"

#include "age/helpers/streams/ADataStream.h"
#include "age/helpers/AHelpersExceptions.h"

ADataStream::ByteOrder ADataStream::systemOrder_ =
    ADataStream::getSystemByteOrder();
//=============================================================================
ADataStream::ByteOrder
ADataStream::getSystemByteOrder()
{
    int16 testValue = 1;
    byte* testByte  = reinterpret_cast<byte*>(&testValue);

    return *testByte ? BO_LITTLE_ENDIAN : BO_BIG_ENDIAN;
}
//=============================================================================
ADataStream::ADataStream()
{
    order_ = BO_LITTLE_ENDIAN;
}
//=============================================================================
ADataStream&
ADataStream::operator << (bool value)
{
    byte boolValue = (byte)value;

    writeInOrder(boolValue);
    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator << (const AXy& xy)
{
    writeInOrder((int64)xy.x);
    writeInOrder((int64)xy.y);
    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator << (const ARect& rect)
{
    operator<<(rect.pos);
    operator<<(rect.size);
    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator << (const std::string& str)
{
    int64 strSize = str.size();

    writeInOrder(strSize);
    write(str.c_str(), strSize);
    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator >> (bool& value)
{
    byte boolValue = 0;
    readInOrder(boolValue);
    value = boolValue;

    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator >> (AXy& xy)
{
    int64 x, y;
    readInOrder(x);
    readInOrder(y);

    xy.x = x;
    xy.y = y;

    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator >> (ARect& rect)
{
    operator>>(rect.pos);
    operator>>(rect.size);
    return *this;
}
//=============================================================================
ADataStream&
ADataStream::operator >> (std::string& str)
{
    int64 strSize = 0;

    readInOrder(strSize);

    // For situations where due to a mistake strSize is VERY BIG value
    // and allocate buffer for this may be a problem
    if(strSize > getBytesAvailableCount())
        throw AIoException(A_EXPOINT);

    std::unique_ptr<char[]> buffer(new char[strSize]);
    read(buffer.get(), strSize);
    str.assign(buffer.get(), strSize);

    return *this;
}
//=============================================================================
