/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef ANATIVEWINDOW_H_INCLUDED
#define ANATIVEWINDOW_H_INCLUDED

#include <string>

#include <X11/Xlib.h>
#include <GL/glx.h>

#include "age/core/types/AAliases.h"
#include "age/core/exceptions/ASystemExceptions.h"

class AEvent;

//=============================================================================
class ANativeWindow
{
public:
// Interface
    ANativeWindow();
   ~ANativeWindow();

    void               setTitle(const std::string& title);
    const std::string& getTitle() const;

    void setCursorVisible(bool isVisible);
    bool isCursorVisible() const;

    void setFullscreen();
    bool isFullscreen() const;

    void         setSize(const ASize& size);
    const ASize& getSize() const;

    const APos& getCursorPos() const;

    void create();
    void update();
    void destroy();

    bool pollEvent(AEvent* event);

private:
// Helpers
    struct DisplayGuard
    {
        Display* display;

        DisplayGuard()
        {
            display = XOpenDisplay(nullptr);
            if(!display)
                throw AWindowException("Failed to open display", A_EXPOINT);
        }

        ~DisplayGuard()
        {
            if(display)
                XCloseDisplay(display);
        }
    };

private:
// Methods
    Display*    getDisplay();
    GLXFBConfig getFbConfig() const;

    void createWindow(GLXFBConfig config);
    void releaseWindow();

    void createContext(GLXFBConfig config);
    void releaseContext();

    bool isGlxVersionSupported(int major, int minor) const;

private:
// Data
    Display*    display_;
    Window      window_;
    GLXContext  context_;
    GLXWindow   glxWindow_;

    std::string title_;
    bool        isCursorVisible_;
    bool        isFullscreen_;
    ASize       size_;
    APos        cursorPos_;
};
//=============================================================================
#endif // ANATIVEWINDOW_H_INCLUDED
