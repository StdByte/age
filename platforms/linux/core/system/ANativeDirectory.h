/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef ANATIVEDIRECTORY_H_INCLUDED
#define ANATIVEDIRECTORY_H_INCLUDED

#include <string>
#include <list>

#include "age/core/system/ADirectory.h"
#include "age/core/types/AAliases.h"

//=============================================================================
class ANativeDirectory
{
public:
// Helpers
    using Entry   = ADirectory::Entry;
    using Entries = ADirectory::Entries;

public:
// Interface
    static bool isExists(const std::string& path);
    static void create(const std::string& path, bool isMayExists);
    static void createRecursively(const std::string& path);
    static void rename(const std::string& oldPath, const std::string& newPath);
    static void removeIfExists(const std::string& path);
    static void getContent(const std::string& path, Entries* entries,
        int32 types);

    // Compute and get prefer directory
    static void setMeta(const std::string& org, const std::string& app);
    static void getPreferDir(ANativeDirectory* dir);

public:
// Interface
    ANativeDirectory()                            = default;
    ANativeDirectory(const ANativeDirectory& dir) = default;

    ANativeDirectory& operator =  (const ANativeDirectory& dir) = default;
    bool              operator == (const ANativeDirectory& dir) const
    {
        return (path_ == dir.path_);
    }

    void               setPath(const std::string& path);
    const std::string& getPath() const { return path_; } // With '/' at end

    void goTo(const std::string& subPath);
    void goUp();

    std::string getEntryPath(const std::string& name, bool isDir) const;

    bool isExists() const                   { return isExists(path_);     }
    void create(bool isMayExists)           { create(path_, isMayExists); }
    void createRecursively()                { createRecursively(path_);   }
    void rename(const std::string& newPath) { rename(path_, newPath);     }
    void removeIfExists()                   { removeIfExists(path_);      }

    void getContent(Entries* entries, int32 types) const
    {
        getContent(path_, entries, types);
    }

    bool isValid() const { return !path_.empty(); }
    void clear() { path_.clear(); }

private:
// Helpers
    using Parts = std::list<std::string>;

private:
// Methods
    static std::string computeHomePath();
    static std::string computeAbsolutePath(const std::string& path);
    static std::string computeCorrectPath(const std::string& path);

    static void        takeRawParts(const std::string& path, Parts* raw);
    static void        takeCorrectedParts(const Parts& raw, Parts* corrected);
    static std::string getPath(const Parts& corrected);

private:
// Data
    static std::string pref_;

private:
// Data
    std::string path_;
};
//=============================================================================
#endif // ANATIVEDIRECTORY_H_INCLUDED
