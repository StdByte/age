/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef ANATIVEFILE_H_INCLUDED
#define ANATIVEFILE_H_INCLUDED

#include <string>

#include "age/core/system/AFile.h"
#include "age/core/types/AAliases.h"

//=============================================================================
class ANativeFile
{
public:
// Helpers
    using FileWhence = AFile::FileWhence;
    using OpenMode   = AFile::OpenMode;

public:
// Interface
    static bool isExists(const std::string& path);
    static void removeIfExists(const std::string& path);
    static void rename(const std::string& oldPath, const std::string& newPath);

public:
// Interface
    ANativeFile();
   ~ANativeFile();

    void open(const std::string& path, int32 mode = AFile::OM_READ_WRITE);
    void close();
    bool isOpen() const { return (fileId_ != -1); }

    const std::string& getPath() const { return path_; }

    int64 setPos(int64 offset, FileWhence whence = AFile::FW_BEGIN);
    int64 getPos() const;

    int64 write(const void* data, int64 len);
    int64 read(void* data, int64 len);

private:
// Methods
    int getNativeModeFlags(int32 mode) const;
    int getNativeWhence(FileWhence whence) const;

private:
// Data
    std::string path_;
    int         fileId_;
};
//=============================================================================
#endif // ANATIVEFILE_H_INCLUDED
