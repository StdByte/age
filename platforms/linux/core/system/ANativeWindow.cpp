/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "core/system/ANativeWindow.h"

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/core/exceptions/ALog.h"

//=============================================================================
ANativeWindow::ANativeWindow() :
    size_(1024, 600)
{
    display_ = getDisplay();
    if(!display_)
        throw ANullptrException(A_EXPOINT);

    window_    = 0;
    context_   = 0;
    glxWindow_ = 0;

    isCursorVisible_ = true;
    isFullscreen_    = false;
}
//=============================================================================
ANativeWindow::~ANativeWindow()
{
    if(!window_)
        return;

    try
    {
        destroy();
    }
    catch(const std::exception& ex)
    {
        ALog::writeException(ex);
    }
}
//=============================================================================
void
ANativeWindow::setTitle(const std::string& title)
{
    if(!window_)
        throw AUnresolvedOperationException(A_EXPOINT);

    title_ = title;
    XStoreName(display_, window_, title.c_str());
}
//=============================================================================
const std::string&
ANativeWindow::getTitle()
    const
{
    if(!window_)
        throw AUnresolvedOperationException(A_EXPOINT);

    return title_;
}
//=============================================================================
void
ANativeWindow::setCursorVisible(bool isVisible)
{
    //TODO --------------------------------------------------------------------------------
}
//=============================================================================
bool
ANativeWindow::isCursorVisible()
    const
{
    return isCursorVisible_;
}
//=============================================================================
void
ANativeWindow::setFullscreen()
{
    if(!window_)
        throw AUnresolvedOperationException(A_EXPOINT);

    //TODO --------------------------------------------------------------------------------
}
//=============================================================================
bool
ANativeWindow::isFullscreen()
    const
{
    return isFullscreen_;
}
//=============================================================================
void
ANativeWindow::setSize(const ASize& size)
{
    if(!window_)
        throw AUnresolvedOperationException(A_EXPOINT);

    //TODO --------------------------------------------------------------------------------
}
//=============================================================================
const ASize&
ANativeWindow::getSize()
    const
{
    return size_;
}
//=============================================================================
const APos&
ANativeWindow::getCursorPos()
    const
{
    //TODO --------------------------------------------------------------------------------

    return cursorPos_;
}
//=============================================================================
void
ANativeWindow::create()
{
    if(window_)
        throw AUnresolvedOperationException(A_EXPOINT);

    if(!isGlxVersionSupported(1, 3))
        throw AWindowException("GLX version not supported", A_EXPOINT);

    GLXFBConfig config = getFbConfig();

    createWindow(config);

    try
    {
        createContext(config);

        try
        {
        }
        catch(...)
        {
            releaseContext();
            throw;
        }
    }
    catch(...)
    {
        releaseWindow();
        throw;
    }
}
//=============================================================================
void
ANativeWindow::update()
{
    if(!glxWindow_)
        throw AUnresolvedOperationException(A_EXPOINT);

    glXSwapBuffers(display_, glxWindow_);
}
//=============================================================================
void
ANativeWindow::destroy()
{
    releaseContext();
    releaseWindow();
}
//=============================================================================
bool
ANativeWindow::pollEvent(AEvent* event)
{
    //TODO --------------------------------------------------------------------------------

    return false;
}
//=============================================================================
Display*
ANativeWindow::getDisplay()
{
    static DisplayGuard guard;
    return guard.display;
}
//=============================================================================
GLXFBConfig
ANativeWindow::getFbConfig()
    const
{
    int attributes[] = {
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_DOUBLEBUFFER,  True,
        GLX_RENDER_TYPE,   GLX_RGBA_BIT,
        GLX_RED_SIZE,      8,
        GLX_GREEN_SIZE,    8,
        GLX_BLUE_SIZE,     8,
        GLX_ALPHA_SIZE,    8,
        GLX_DEPTH_SIZE,    24,
        None
    };

    int configsCount = 0;

    GLXFBConfig* configs = glXChooseFBConfig(display_,
        XDefaultScreen(display_), attributes, &configsCount);

    if(!configs)
    {
        throw AWindowException("Frame buffer attributes not supported",
            A_EXPOINT);
    }

    GLXFBConfig config = configs[0];
    XFree(configs);
    return config;
}
//=============================================================================
void
ANativeWindow::createWindow(GLXFBConfig config)
{
    XVisualInfo* info = glXGetVisualFromFBConfig(display_, config);
    if(!info)
        throw AWindowException("Failed to get visual info", A_EXPOINT);

    try
    {
        Window rootWindow = RootWindow(display_, info->screen);

        XSetWindowAttributes xWinAttribs;
        xWinAttribs.event_mask = ExposureMask | KeyPressMask;

        window_ = XCreateWindow(display_, rootWindow, 0, 0, size_.x, size_.y,
            0, info->depth, InputOutput, info->visual, CWEventMask,
            &xWinAttribs);

        if(!window_)
            throw AWindowException("Failed to create window", A_EXPOINT);

        try
        {
            if(!XMapWindow(display_, window_))
                throw AWindowException("Failed to show window", A_EXPOINT);
        }
        catch(...)
        {
            releaseWindow();
            throw;
        }
    }
    catch(...)
    {
        XFree(info);
        throw;
    }

    XFree(info);
}
//=============================================================================
void
ANativeWindow::releaseWindow()
{
    if(!window_)
        return;

    XDestroyWindow(display_, window_);
    window_ = 0;
}
//=============================================================================
void
ANativeWindow::createContext(GLXFBConfig config)
{
    if(!window_)
        throw AUnresolvedOperationException(A_EXPOINT);

    context_ = glXCreateNewContext(display_, config, GLX_RGBA_TYPE, nullptr,
        True);
    if(!context_)
        throw AWindowException("Failed to create gl context", A_EXPOINT);

    try
    {
        glxWindow_ = glXCreateWindow(display_, config, window_, nullptr);
        if(!glxWindow_)
            throw AWindowException("Failed to create glx window", A_EXPOINT);

        if(!glXMakeContextCurrent(display_, glxWindow_, glxWindow_, context_))
        {
            throw AWindowException("Failed to bind context to window",
                A_EXPOINT);
        }
    }
    catch(...)
    {
        releaseContext();
        throw;
    }
}
//=============================================================================
void
ANativeWindow::releaseContext()
{
    if(!context_)
        return;

    glXDestroyWindow(display_, glxWindow_);
    glXDestroyContext(display_, context_);

    glxWindow_ = 0;
    context_   = 0;
}
//=============================================================================
bool
ANativeWindow::isGlxVersionSupported(int major, int minor)
    const
{
    int glxMinor = 0;
    int glxMajor = 0;

    if(!glXQueryVersion(display_, &glxMinor, &glxMajor))
        throw AWindowException("Failed to check glx version", A_EXPOINT);

    return (glxMajor > major || (glxMajor == major && glxMinor >= minor));
}
//=============================================================================
