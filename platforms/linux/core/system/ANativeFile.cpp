/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/core/exceptions/ASystemExceptions.h"
#include "age/core/exceptions/ALog.h"

#include "core/system/ANativeFile.h"

//=============================================================================
bool
ANativeFile::isExists(const std::string& path)
{
    struct stat st;

    if(::stat(path.c_str(), &st))
        return false;

    return S_ISREG(st.st_mode);
}
//=============================================================================
void
ANativeFile::removeIfExists(const std::string& path)
{
    if(::unlink(path.c_str()))
    {
        if(errno != ENOENT)
            throw AFailedRemoveFileException(path, A_EXPOINT);
    }
}
//=============================================================================
void
ANativeFile::rename(const std::string& oldPath, const std::string& newPath)
{
    if(::rename(oldPath.c_str(), newPath.c_str()))
        throw AFailedRenameFileException(oldPath, newPath, A_EXPOINT);
}
//=============================================================================
ANativeFile::ANativeFile()
{
    fileId_ = -1;
}
//=============================================================================
ANativeFile::~ANativeFile()
{
    try
    {
        close();
    }
    catch(const std::exception& ex)
    {
        ALog::writeException(ex);
    }
}
//=============================================================================
void
ANativeFile::open(const std::string& path, int32 mode)
{
    close();

    int    flags = getNativeModeFlags(mode);
    mode_t attrs = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH;

    if(mode & AFile::OM_WRITE)
        fileId_ = ::open(path.c_str(), flags, attrs);
    else fileId_ = ::open(path.c_str(), flags);

    if(fileId_ == -1)
        throw AFailedOpenFileException(path, A_EXPOINT);

    try
    {
        path_ = path;
    }
    catch(...)
    {
        close();
        throw;
    }

    if(mode & AFile::OM_ATEND)
        setPos(0, AFile::FW_END);
}
//=============================================================================
void
ANativeFile::close()
{
    if(fileId_ == -1)
        return;

    ::close(fileId_);
    fileId_ = -1;

    path_.clear();
}
//=============================================================================
int64
ANativeFile::setPos(int64 offset, FileWhence whence)
{
    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    int   nativeWhence = getNativeWhence(whence);
    off_t result       = ::lseek(fileId_, offset, nativeWhence);

    if(result == -1)
        throw AFailedMoveFileCursorException(path_, A_EXPOINT);

    return result;
}
//=============================================================================
int64
ANativeFile::getPos()
    const
{
    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    off_t result = ::lseek(fileId_, 0, SEEK_CUR);
    if(result == -1)
        throw AFailedMoveFileCursorException(path_, A_EXPOINT);

    return result;
}
//=============================================================================
int64
ANativeFile::write(const void* data, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    ssize_t count = ::write(fileId_, data, len);

    if(count == -1)
        throw AFailedWriteFileException(path_, A_EXPOINT);

    return count;
}
//=============================================================================
int64
ANativeFile::read(void* data, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    ssize_t count = ::read(fileId_, data, len);

    if(count == -1)
        throw AFailedReadFileException(path_, A_EXPOINT);

    return count;
}
//=============================================================================
int
ANativeFile::getNativeModeFlags(int32 mode)
    const
{
    int  flags   = 0;
    bool isRead  = mode & AFile::OM_READ;
    bool isWrite = mode & AFile::OM_WRITE;
    bool isTrunc = mode & AFile::OM_TRUNC;

    if(isRead && isWrite)
        flags |= O_RDWR;
    else
    {
        if(isRead)
            flags |= O_RDONLY;
        else if(isWrite)
            flags |= O_WRONLY;
        else throw ABadParametersException(A_EXPOINT);
    }

    if(isWrite)
        flags |= O_CREAT;

    if(isTrunc)
    {
        if(isWrite)
            flags |= O_TRUNC;
        else throw ABadParametersException(A_EXPOINT);
    }

    return flags;
}
//=============================================================================
int
ANativeFile::getNativeWhence(FileWhence whence)
    const
{
    switch(whence)
    {
        case AFile::FW_BEGIN:   return SEEK_SET;
        case AFile::FW_CURRENT: return SEEK_CUR;
        case AFile::FW_END:     return SEEK_END;
        default:
            throw AUndefinedBehaviorException(A_EXPOINT);
    }
}
//=============================================================================
