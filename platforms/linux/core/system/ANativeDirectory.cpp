/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <memory>

#include <pwd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/core/exceptions/ASystemExceptions.h"

#include "core/system/ANativeDirectory.h"

std::string ANativeDirectory::pref_;
//=============================================================================
bool
ANativeDirectory::isExists(const std::string& path)
{
    struct stat st;

    if(::stat(path.c_str(), &st))
        return false;

    return S_ISDIR(st.st_mode);
}
//=============================================================================
void
ANativeDirectory::create(const std::string& path, bool isMayExists)
{
    mode_t mode   = (S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    int    result = ::mkdir(path.c_str(), mode);

    if(result == -1)
    {
        if(isMayExists && errno == EEXIST)
            return;

        throw AFailedCreateDirException(path, A_EXPOINT);
    }
}
//=============================================================================
void
ANativeDirectory::createRecursively(const std::string& path)
{
    std::string curPath;
    char        curChar = path[0];

    for(int64 i = 1; curChar != '\0'; ++i)
    {
        curPath += curChar;
        curChar  = path[i];

        if(curChar == '/')
            create(curPath, true);
    }

    if(curPath.back() != '/')
        create(curPath, true);
}
//=============================================================================
void
ANativeDirectory::rename(const std::string& oldPath,
        const std::string& newPath)
{
    if(oldPath.size() == 0 || newPath.size() == 0)
        throw AFailedRenameDirException(oldPath, newPath, A_EXPOINT);

    int result = ::rename(
        oldPath.back() == '/' ? oldPath.c_str() : (oldPath + '/').c_str(),
        newPath.c_str()
    );

    if(result == -1)
        throw AFailedRenameDirException(oldPath, newPath, A_EXPOINT);
}
//=============================================================================
void
ANativeDirectory::removeIfExists(const std::string& path)
{
    int result = rmdir(path.c_str());

    if(result == -1 && errno != ENOENT)
        throw AFailedRemoveDirException(path, A_EXPOINT);
}
//=============================================================================
void
ANativeDirectory::getContent(const std::string& path, Entries* entries,
        int32 types)
{
    if(!entries)
        throw ANullptrException(A_EXPOINT);

    DIR*           dir    = nullptr;
    struct dirent* dirent = nullptr;

    dir = ::opendir(path.c_str());
    if(!dir)
        throw AFailedOpenDirException(path, A_EXPOINT);

    try
    {
        Entry entry;

        for(dirent = ::readdir(dir); dirent; dirent = ::readdir(dir))
        {
                 if(dirent->d_type == DT_DIR) entry.type = ADirectory::ET_DIR;
            else if(dirent->d_type == DT_REG) entry.type = ADirectory::ET_FILE;
            else continue;

            if(!(entry.type & types))
                continue;

            entry.name = dirent->d_name;

            if(entry.name == "." || entry.name == "..")
                continue;

            entries->push_back(std::move(entry));
        }

        ::closedir(dir);
    }
    catch(...)
    {
        ::closedir(dir);
        throw;
    }
}
//=============================================================================
void
ANativeDirectory::setMeta(const std::string& org, const std::string& app)
{
    std::string newPref = computeHomePath();
    newPref += ".local/share/";
    newPref += app;
    newPref += '/';

    if(!isExists(newPref))
        createRecursively(newPref);

    pref_ = std::move(newPref);
}
//=============================================================================
void
ANativeDirectory::getPreferDir(ANativeDirectory* dir)
{
    if(!dir)
        throw ANullptrException(A_EXPOINT);

    dir->path_ = pref_;
}
//=============================================================================
void
ANativeDirectory::setPath(const std::string& path)
{
    std::string newPath;
    newPath = computeAbsolutePath(path);
    newPath = computeCorrectPath(newPath);

    path_ = std::move(newPath);
}
//=============================================================================
void
ANativeDirectory::goTo(const std::string& subPath)
{
    std::string newPath;
    newPath = (path_ + subPath);
    newPath = computeCorrectPath(newPath);

    path_ = std::move(newPath);
}
//=============================================================================
void
ANativeDirectory::goUp()
{
    if(path_.size() < 2)    // If path empty or "/"
        throw AUnresolvedOperationException(A_EXPOINT);

    int64 pos = (path_.size() - 2); // Skip '/' at the end

    pos   = path_.rfind('/', pos);      // Find last '/' in the path
    path_ = path_.substr(0, pos + 1);   // Get new path with
}
//=============================================================================
std::string
ANativeDirectory::getEntryPath(const std::string& name, bool isDir)
    const
{
    std::string result = path_;
    result += name;

    if(isDir && (result.back() != '/'))
        result += '/';

    return result;
}
//=============================================================================
std::string
ANativeDirectory::computeHomePath()
{
    const int64 DEFAULT_BUFFER_SIZE = 16384;

    int64 bufferSize = ::sysconf(_SC_GETPW_R_SIZE_MAX);
    if(bufferSize == -1)
        bufferSize = DEFAULT_BUFFER_SIZE;

    auto buffer = std::make_unique<char[]>(bufferSize);

    struct passwd  pwd;
    struct passwd* result;

    ::getpwuid_r(::getuid(), &pwd, buffer.get(), bufferSize, &result);
    if(!result)
        throw AFailedGetSystemPathException(A_EXPOINT);

    std::string path = pwd.pw_dir;
    if(path.back() != '/')
        path += '/';

    return path;
}
//=============================================================================
std::string
ANativeDirectory::computeAbsolutePath(const std::string& path)
{
    if(path.empty())
        throw ABadParametersException(A_EXPOINT);

    if(path.front() == '/')
        return path;

    thread_local std::string cachedWorkingDirPath;

    if(cachedWorkingDirPath.empty())
    {
        std::unique_ptr<char[]> crn(::get_current_dir_name());
        if(!crn)
            throw AFailedGetSystemPathException(A_EXPOINT);

        cachedWorkingDirPath = crn.get();

        if(cachedWorkingDirPath.back() != '/')
            cachedWorkingDirPath += '/';
    }

    return cachedWorkingDirPath + path;
}
//=============================================================================
std::string
ANativeDirectory::computeCorrectPath(const std::string& path)
{
    Parts raw;
    Parts corrected;

    takeRawParts(path, &raw);
    takeCorrectedParts(raw, &corrected);

    return getPath(corrected);
}
//=============================================================================
void
ANativeDirectory::takeRawParts(const std::string& path, Parts* raw)
{
    if(!raw)
        throw ANullptrException(A_EXPOINT);

    std::string part;

    for(char c : path)
    {
        if(c == '/')
        {
            raw->push_back(std::move(part));
            part.clear();
            continue;
        }

        part += c;
    }

    raw->push_back(std::move(part));
}
//=============================================================================
void
ANativeDirectory::takeCorrectedParts(const Parts& raw, Parts* corrected)
{
    if(!corrected)
        throw ANullptrException(A_EXPOINT);

    for(const std::string& part : raw)
    {
        if(part.empty() || part == ".")
            continue;

        if(part == "..")
        {
            if(corrected->empty())
                throw AUnresolvedOperationException(A_EXPOINT);

            corrected->pop_back();
            continue;
        }

        corrected->push_back(part);
    }
}
//=============================================================================
std::string
ANativeDirectory::getPath(const Parts& corrected)
{
    std::string result;

    result += '/';
    for(const std::string& part : corrected)
    {
        result += part;
        result += '/';
    }

    return result;
}
//=============================================================================
