/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <algorithm>
#include <memory>

#include <windows.h>
#include <Shlobj.h>

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/core/exceptions/ASystemExceptions.h"

#include "core/system/ANativeDirectory.h"

std::wstring ANativeDirectory::nativePref_;
std::string  ANativeDirectory::commonPref_;
//=============================================================================
void
ANativeDirectory::setMeta(const std::string& org, const std::string& app)
{
    std::wstring newPref = computePreferRootPath();
    newPref += toNative(org);
    newPref += L'/';
    newPref += toNative(app);
    newPref += L'/';

    if(!isExists(newPref))
        createRecursively(newPref);

    commonPref_.clear();
    nativePref_ = std::move(newPref);
}
//=============================================================================
void
ANativeDirectory::getPreferDir(ANativeDirectory* dir)
{
    if(!dir)
        throw ANullptrException(A_EXPOINT);

    if(commonPref_.empty())
        commonPref_ = toCommon(nativePref_);

    dir->commonPath_.clear();
    dir->nativePath_ = nativePref_;
    dir->commonPath_ = commonPref_;
}
//=============================================================================
ANativeDirectory&
ANativeDirectory::operator = (const ANativeDirectory& dir)
{
    if(this == &dir)
        return *this;

    commonPath_.clear();
    nativePath_ = dir.nativePath_;
    commonPath_ = dir.commonPath_;

    return *this;
}
//=============================================================================
void
ANativeDirectory::setPath(const std::string& path)
{
    std::wstring newPath;
    newPath = toNative(path);
    newPath = computeAbsolutePath(newPath);
    newPath = computeCorrectPath(newPath);

    commonPath_.clear();
    nativePath_ = std::move(newPath);
}
//=============================================================================
void
ANativeDirectory::goTo(const std::string& subPath)
{
    std::wstring newPath;
    newPath = (nativePath_ + toNative(subPath));
    newPath = computeCorrectPath(newPath);

    commonPath_.clear();
    nativePath_ = std::move(newPath);
}
//=============================================================================
void
ANativeDirectory::goUp()
{
    if(nativePath_.size() < 4)  // If path empty or "X:/"
        throw AUnresolvedOperationException(A_EXPOINT);

    int64 pos = (nativePath_.size() - 2);   // Skip '/' at the end
    pos = nativePath_.rfind(L'/', pos);     // Find last '/' in the path

    commonPath_.clear();
    nativePath_ = nativePath_.substr(0, pos + 1);   // Get new path
}
//=============================================================================
std::string
ANativeDirectory::getEntryPath(const std::string& name, bool isDir)
    const
{
    applyCommonPath();

    std::string result = commonPath_;
    result += name;

    if(isDir && (result.back() != '/'))
        result += '/';

    return result;
}
//=============================================================================
bool
ANativeDirectory::isExists(const std::wstring& path)
{
    DWORD attr = ::GetFileAttributesW(path.c_str());

    if(attr == INVALID_FILE_ATTRIBUTES)
        return false;

    return (attr & FILE_ATTRIBUTE_DIRECTORY);
}
//=============================================================================
void
ANativeDirectory::create(const std::wstring& path, bool isMayExists)
{
    if(::CreateDirectoryW(path.c_str(), nullptr))
        return;

    DWORD result = ::GetLastError();

    if(isMayExists && result == ERROR_ALREADY_EXISTS)
        return;

    if(isMayExists && result == ERROR_ACCESS_DENIED && isExists(path))
        return;

    throw AFailedCreateDirException(toCommon(path), A_EXPOINT);
}
//=============================================================================
void
ANativeDirectory::createRecursively(const std::wstring& path)
{
    std::wstring curPath;
    wchar_t      curChar = path[0];

    for(int64 i = 1; curChar != L'\0'; ++i)
    {
        curPath += curChar;
        curChar  = path[i];

        if(curChar == L'/')
            create(curPath, true);
    }

    if(curPath.back() != L'/')
        create(curPath, true);
}
//=============================================================================
void
ANativeDirectory::rename(const std::wstring& oldPath,
        const std::wstring& newPath)
{
    bool result = ::MoveFileW(
        oldPath.back() == L'/' ? oldPath.c_str() : (oldPath + L'/').c_str(),
        newPath.c_str()
    );

    if(!result)
    {
        throw AFailedRenameDirException(toCommon(oldPath), toCommon(newPath),
            A_EXPOINT);
    }
}
//=============================================================================
void
ANativeDirectory::removeIfExists(const std::wstring& path)
{
    if(::RemoveDirectoryW(path.c_str()))
        return;

    if(::GetLastError() == ERROR_FILE_NOT_FOUND)
        return;

    throw AFailedRemoveDirException(toCommon(path), A_EXPOINT);
}
//=============================================================================
void
ANativeDirectory::getContent(const std::wstring& path, Entries* entries,
        int32 types)
{
    if(!entries)
        throw ANullptrException(A_EXPOINT);

    WIN32_FIND_DATAW dirEntry;
    std::wstring     mask      = getFindMask(path);
    HANDLE           dirHandle = ::FindFirstFileW(mask.c_str(), &dirEntry);

    if(dirHandle == INVALID_HANDLE_VALUE)
        throw AFailedOpenDirException(toCommon(path), A_EXPOINT);

    try
    {
        Entry entry;

        do
        {
            if(dirEntry.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                entry.type = ADirectory::ET_DIR;
            else entry.type = ADirectory::ET_FILE;

            if(!(entry.type & types))
                continue;

            entry.name = toCommon(dirEntry.cFileName);

            if(entry.name == "." || entry.name == "..")
                continue;

            entries->push_back(std::move(entry));

        } while(::FindNextFileW(dirHandle, &dirEntry));

        ::FindClose(dirHandle);
    }
    catch(...)
    {
        ::FindClose(dirHandle);
        throw;
    }
}
//=============================================================================
std::wstring
ANativeDirectory::computePreferRootPath()
{
    struct PwstrDeleter
    {
        void operator()(void* ptr) { ::CoTaskMemFree(ptr); }
    };

    PWSTR   path   = nullptr;
    HRESULT result = ::SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0,
        nullptr, &path);

    if(result != S_OK)
        throw AFailedGetSystemPathException(A_EXPOINT);

    std::unique_ptr<WCHAR, PwstrDeleter> pathPtr(path);

    std::wstring prefPath = pathPtr.get();
    replaceSlashes(&prefPath);

    if(prefPath.back() != L'/')
        prefPath += L'/';

    return prefPath;
}
//=============================================================================
std::wstring
ANativeDirectory::computeAbsolutePath(const std::wstring& path)
{
    if(path.empty())
        throw ABadParametersException(A_EXPOINT);

    if(path.size() > 1 && path[1] == L':')
        return path;

    thread_local std::wstring cachedWorkingDirPath;

    if(cachedWorkingDirPath.empty())
    {
        DWORD                      length = GetCurrentDirectoryW(0, nullptr);
        std::unique_ptr<wchar_t[]> buffer(new wchar_t[length]);
        GetCurrentDirectoryW(length, buffer.get());

        cachedWorkingDirPath = buffer.get();
        replaceSlashes(&cachedWorkingDirPath);

        if(cachedWorkingDirPath.back() != L'/')
            cachedWorkingDirPath += L'/';
    }

    return cachedWorkingDirPath + path;
}
//=============================================================================
std::wstring
ANativeDirectory::computeCorrectPath(const std::wstring& path)
{
    Parts raw;
    Parts corrected;

    takeRawParts(path, &raw);
    takeCorrectedParts(raw, &corrected);

    return getPath(corrected);
}
//=============================================================================
void
ANativeDirectory::takeRawParts(const std::wstring& path, Parts* raw)
{
    if(!raw)
        throw ANullptrException(A_EXPOINT);

    std::wstring part;

    for(wchar_t c : path)
    {
        if(c == L'/')
        {
            raw->push_back(std::move(part));
            part.clear();
            continue;
        }

        part += c;
    }

    raw->push_back(std::move(part));
}
//=============================================================================
void
ANativeDirectory::takeCorrectedParts(const Parts& raw, Parts* corrected)
{
    if(!corrected)
        throw ANullptrException(A_EXPOINT);

    for(const std::wstring& part : raw)
    {
        if(part.empty() || part == L".")
            continue;

        if(part == L"..")
        {
            if(corrected->empty())
                throw AUnresolvedOperationException(A_EXPOINT);

            corrected->pop_back();
            continue;
        }

        corrected->push_back(part);
    }
}
//=============================================================================
std::wstring
ANativeDirectory::getPath(const Parts& corrected)
{
    std::wstring result;

    for(const std::wstring& part : corrected)
    {
        result += part;
        result += L'/';
    }

    return result;
}
//=============================================================================
void
ANativeDirectory::replaceSlashes(std::wstring* path)
{
    if(!path)
        throw ANullptrException(A_EXPOINT);

    std::replace(path->begin(), path->end(), L'\\', L'/');
}
//=============================================================================
std::wstring
ANativeDirectory::getFindMask(const std::wstring& path)
{
    std::wstring mask = path;

    if(mask.back() != L'/')
        mask += L'/';

    mask += L'*';

    return mask;
}
//=============================================================================
