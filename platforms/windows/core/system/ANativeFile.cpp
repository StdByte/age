/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "age/core/exceptions/ALogicExceptions.h"
#include "age/core/exceptions/ASystemExceptions.h"
#include "age/core/exceptions/ALog.h"

#include "core/system/ANativeFile.h"

//=============================================================================
ANativeFile::ANativeFile()
{
    file_ = INVALID_HANDLE_VALUE;
}
//=============================================================================
ANativeFile::~ANativeFile()
{
    try
    {
        close();
    }
    catch(const std::exception& ex)
    {
        ALog::writeException(ex);
    }
}
//=============================================================================
void
ANativeFile::open(const std::string& path, int32 mode)
{
    close();

    std::wstring nativePath = toNative(path);

    DWORD generic             = getGeneric(mode);
    DWORD creationDisposition = getCreationDisposition(mode);

    file_ = ::CreateFileW(nativePath.c_str(), generic,
        FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, creationDisposition,
        FILE_ATTRIBUTE_NORMAL, nullptr);

    if(file_ == INVALID_HANDLE_VALUE)
        throw AFailedOpenFileException(path, A_EXPOINT);

    try
    {
        commonPath_ = path;
    }
    catch(...)
    {
        close();
        throw;
    }

    if(mode & AFile::OM_ATEND)
        setPos(0, AFile::FW_END);
}
//=============================================================================
void
ANativeFile::close()
{
    if(file_ == INVALID_HANDLE_VALUE)
        return;

    CloseHandle(file_);
    file_ = INVALID_HANDLE_VALUE;

    commonPath_.clear();
}
//=============================================================================
int64
ANativeFile::setPos(int64 offset, FileWhence whence)
{
    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    DWORD         moveMethod = getMoveMethod(whence);
    LARGE_INTEGER result     = getLargeInteger(0);
    LARGE_INTEGER distance   = getLargeInteger(offset);

    if(!::SetFilePointerEx(file_, distance, &result, moveMethod))
        throw AFailedMoveFileCursorException(commonPath_, A_EXPOINT);

    return result.QuadPart;
}
//=============================================================================
int64
ANativeFile::getPos()
    const
{
    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    LARGE_INTEGER result   = getLargeInteger(0);
    LARGE_INTEGER distance = getLargeInteger(0);

    if(!::SetFilePointerEx(file_, distance, &result, FILE_CURRENT))
        throw AFailedMoveFileCursorException(commonPath_, A_EXPOINT);

    return result.QuadPart;
}
//=============================================================================
int64
ANativeFile::write(const void* data, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    DWORD result = 0;

    if(!::WriteFile(file_, data, len, &result, nullptr))
        throw AFailedWriteFileException(commonPath_, A_EXPOINT);

    return result;
}
//=============================================================================
int64
ANativeFile::read(void* data, int64 len)
{
    if(!data)
        throw ANullptrException(A_EXPOINT);

    if(!isOpen())
        throw AUnresolvedOperationException(A_EXPOINT);

    DWORD result = 0;

    if(!::ReadFile(file_, data, len, &result, nullptr))
        throw AFailedReadFileException(commonPath_, A_EXPOINT);

    return result;
}
//=============================================================================
bool
ANativeFile::isExists(const std::wstring& path)
{
    DWORD attr = ::GetFileAttributesW(path.c_str());

    if(attr == INVALID_FILE_ATTRIBUTES)
        return false;

    return !(attr & FILE_ATTRIBUTE_DIRECTORY);
}
//=============================================================================
void
ANativeFile::removeIfExists(const std::wstring& path)
{
    if(::DeleteFileW(path.c_str()))
        return;

    if(::GetLastError() == ERROR_FILE_NOT_FOUND)
        return;

    throw AFailedRemoveFileException(toCommon(path), A_EXPOINT);
}
//=============================================================================
void
ANativeFile::rename(const std::wstring& oldPath, const std::wstring& newPath)
{
    if(!::MoveFileW(oldPath.c_str(), newPath.c_str()))
    {
        throw AFailedRenameFileException(toCommon(oldPath), toCommon(newPath),
            A_EXPOINT);
    }
}
//=============================================================================
DWORD
ANativeFile::getGeneric(int32 mode)
    const
{
    bool isRead  = mode & AFile::OM_READ;
    bool isWrite = mode & AFile::OM_WRITE;

    if(!isRead && !isWrite)
        throw ABadParametersException(A_EXPOINT);

    DWORD result = 0;

    if(isRead)
        result |= GENERIC_READ;

    if(isWrite)
        result |= GENERIC_WRITE;

    return result;
}
//=============================================================================
DWORD
ANativeFile::getCreationDisposition(int32 mode)
    const
{
    bool isWrite = mode & AFile::OM_WRITE;
    bool isTrunc = mode & AFile::OM_TRUNC;

    if(!isWrite && !isTrunc)
        return OPEN_EXISTING;

    if(isWrite && !isTrunc)
        return OPEN_ALWAYS;

    if(isWrite && isTrunc)
        return CREATE_ALWAYS;

    throw ABadParametersException(A_EXPOINT);
}
//=============================================================================
DWORD
ANativeFile::getMoveMethod(FileWhence whence)
    const
{
    switch(whence)
    {
        case AFile::FW_BEGIN:   return FILE_BEGIN;
        case AFile::FW_CURRENT: return FILE_CURRENT;
        case AFile::FW_END:     return FILE_END;
        default:
            throw AUndefinedBehaviorException(A_EXPOINT);
    }
}
//=============================================================================
