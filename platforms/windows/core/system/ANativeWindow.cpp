/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "core/system/ANativeWindow.h"

//=============================================================================
ANativeWindow::ANativeWindow()
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
ANativeWindow::~ANativeWindow()
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
void
ANativeWindow::setTitle(const std::string& title)
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
const std::string&
ANativeWindow::getTitle()
    const
{
    //TODO ---------------------------------------------------------------------------------

    return title_;
}
//=============================================================================
void
ANativeWindow::setCursorVisible(bool isVisible)
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
bool
ANativeWindow::isCursorVisible()
    const
{
    //TODO ---------------------------------------------------------------------------------

    return true;
}
//=============================================================================
void
ANativeWindow::setFullscreen(bool isEnabled)
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
bool
ANativeWindow::isFullscreen()
    const
{
    //TODO ---------------------------------------------------------------------------------

    return false;
}
//=============================================================================
void
ANativeWindow::setSize(const ASize& size)
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
const ASize&
ANativeWindow::getSize()
    const
{
    //TODO ---------------------------------------------------------------------------------

    return size_;
}
//=============================================================================
const APos&
ANativeWindow::getCursorPos()
    const
{
    //TODO ---------------------------------------------------------------------------------

    return cursorPos_;
}
//=============================================================================
void
ANativeWindow::create()
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
void
ANativeWindow::update()
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
void
ANativeWindow::destroy()
{
    //TODO ---------------------------------------------------------------------------------
}
//=============================================================================
bool
ANativeWindow::pollEvent(AEvent* event)
{
    //TODO ---------------------------------------------------------------------------------

    return false;
}
//=============================================================================
