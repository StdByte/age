/*
Copyright (C) 2017 Kostyuk Anton <stdbyte@gmail.com>

This file is part of AGE.

AGE is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

AGE is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with AGE; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef ANATIVEDIRECTORY_H_INCLUDED
#define ANATIVEDIRECTORY_H_INCLUDED

#include <string>
#include <list>

#include "age/core/system/ADirectory.h"
#include "age/core/types/AAliases.h"
#include "age/core/AUnicode.h"

//=============================================================================
class ANativeDirectory
{
public:
// Helpers
    using Entry   = ADirectory::Entry;
    using Entries = ADirectory::Entries;

public:
// Interface
    static bool isExists(const std::string& path)
    {
        return isExists(toNative(path));
    }

    static void create(const std::string& path, bool isMayExists)
    {
        create(toNative(path), isMayExists);
    }

    static void createRecursively(const std::string& path)
    {
        createRecursively(toNative(path));
    }

    static void rename(const std::string& oldPath, const std::string& newPath)
    {
        rename(toNative(oldPath), toNative(newPath));
    }

    static void removeIfExists(const std::string& path)
    {
        removeIfExists(toNative(path));
    }

    static void getContent(const std::string& path, Entries* entries,
            int32 types)
    {
        getContent(toNative(path), entries, types);
    }

    // Compute and get prefer directory
    static void setMeta(const std::string& org, const std::string& app);
    static void getPreferDir(ANativeDirectory* dir);

public:
// Interface
    ANativeDirectory()                            = default;
    ANativeDirectory(const ANativeDirectory& dir) = default;

    ANativeDirectory& operator =  (const ANativeDirectory& dir);
    bool              operator == (const ANativeDirectory& dir) const
    {
        return (nativePath_ == dir.nativePath_);
    }

    void               setPath(const std::string& path);
    const std::string& getPath() const // With '/' at end
    {
        applyCommonPath();
        return commonPath_;
    }

    void goTo(const std::string& subPath);
    void goUp();

    std::string getEntryPath(const std::string& name, bool isDir) const;

    bool isExists() const         { return isExists(nativePath_);     }
    void create(bool isMayExists) { create(nativePath_, isMayExists); }
    void createRecursively()      { createRecursively(nativePath_);   }
    void removeIfExists()         { removeIfExists(nativePath_);      }

    void rename(const std::string& newPath)
    {
        rename(nativePath_, toNative(newPath));
    }

    void getContent(Entries* entries, int32 types) const
    {
        getContent(nativePath_, entries, types);
    }

    bool isValid() const { return !nativePath_.empty(); }
    void clear()
    {
        nativePath_.clear();
        commonPath_.clear();
    }

private:
// Helpers
    using Parts = std::list<std::wstring>;

private:
// Methods
    static bool isExists(const std::wstring& path);
    static void create(const std::wstring& path, bool isMayExists);
    static void createRecursively(const std::wstring& path);
    static void rename(const std::wstring& oldPath,
        const std::wstring& newPath);
    static void removeIfExists(const std::wstring& path);
    static void getContent(const std::wstring& path, Entries* entries,
        int32 types);

    static std::wstring computePreferRootPath();
    static std::wstring computeAbsolutePath(const std::wstring& path);
    static std::wstring computeCorrectPath(const std::wstring& path);

    static void         takeRawParts(const std::wstring& path, Parts* raw);
    static void         takeCorrectedParts(const Parts& raw, Parts* corrected);
    static std::wstring getPath(const Parts& corrected);

    static void         replaceSlashes(std::wstring* path);
    static std::wstring getFindMask(const std::wstring& path);

    static std::wstring toNative(const std::string& commonPath)
    {
        return AUnicode::toUtf16<wchar_t>(commonPath);
    }

    static std::string toCommon(const std::wstring& nativePath)
    {
        return AUnicode::fromUtf16<wchar_t>(nativePath);
    }

private:
// Methods
    void applyCommonPath() const
    {
        if(commonPath_.empty())
            commonPath_ = toCommon(nativePath_);
    }

private:
// Data
    static std::wstring nativePref_;
    static std::string  commonPref_;

private:
// Data
            std::wstring nativePath_;
    mutable std::string  commonPath_;
};
//=============================================================================
#endif // ANATIVEDIRECTORY_H_INCLUDED
