#include <iostream>
#include <exception>

#include "age/core/exceptions/ALog.h"
#include "age/core/system/AWindow.h"

#include <GL/gl.h>

//=============================================================================
int
main()
{
    try
    {
        AWindow& window = AWindow::getInstance();
        window.create();

        glClearColor(0, 0, 0, 0);

        for(int i = 0; true; ++i)
        {
            glClear(GL_COLOR_BUFFER_BIT);
            window.update();
        }

        return 0;
    }
    catch(const std::exception& ex)
    {
        ALog::writeException(ex);
        return 1;
    }
}
//=============================================================================
