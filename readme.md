# AGE #

### What is you need for compilation? ###
* For Linux: GCC
* For Windows: MinGW-w64 and MSYS (MSYS not required if Code::Blocks used)

### Command for compilation: ###
* For Linux: make -f linux
* Fow Windows: mingw32-make.exe -f windows

### Linux dependencies: ###
* X11 (libX11.so and headers)
* openGL (libGL.so and headers)

This project presents a dynamic library with exported classes.
If you want to use it, you must compile the library and your own project with the same compiler.
